﻿using System;
using System.IO;
using System.Linq;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using System.Configuration;
using Nuance.Omnipage.SampleCode.Types;

namespace Com.BancoAzteca.OCR
{
    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        public const int Paralellismlevel = 500;

        /// <summary>
        /// The program entry point.
        /// </summary>
        /// <param name="args">
        /// The command line arguments
        /// </param>
        public static void Main(string[] args)
        {
            OCRProcessor ocr = new OCRProcessor();
            //try
            //{
            //    
            //    const string file1 = @"C:\Users\fahernandezl\Documents\ocrtest\800 Imagenes\BranchTest\Imagen119_1.jpg";
            //    const string file2 = @"C:\Users\fahernandezl\Documents\ocrtest\800 Imagenes\BranchTest\Imagen119_2.jpg";
            //    Console.WriteLine(ocr.ExtractXmlFromImage(file1, file2));
            //    Console.ReadLine();
            //}
            //catch (CSDKException e)
            //{
            //    Console.WriteLine(e);
            //}
            const string filePath = @"C:\Imagenes3230";
            //var res = ocr.ExtractXmlFromImage(@"C:\Users\fahernandezl\Desktop\Failing\IMAGEN_1.jpg", @"C:\Users\fahernandezl\Desktop\Failing\IMAGEN_2.jpg", "Credencial IFE");
            ocr.CreateXMLToUpload(filePath);

            //ocr.DoExtractFromImage(@"C:\Users\fahernandezl\Desktop\1A.jpg");
            //if (res == "1")
            //{
            //    Console.WriteLine("OCR Deshabilitado");
            //}
            //if (res == "2")
            //{
            //    Console.WriteLine("Error en OCR");
            //}

            //if (res == "3")
            //{
            //    Console.WriteLine("No existe plantilla");
            //}
            //if (res == "4")
            //{
            //    Console.WriteLine("El documento escaneado no corresponde al solicitado");
            //}
            //if (res == "5")
            //{
            //    Console.WriteLine("Se digitalizó el mismo lado del documento.");
            //}
        }

        public static SettingCollection[] GetSettingsPackage()
        {
            return Directory.EnumerateFiles(@".\settings\", "*.sts")
                .Select(sts =>
                {
                    var settings = new SettingCollection();
                    settings.Load(sts);

                    return new SettingCollection(settings);
                })
                .ToArray();

            //{
            // new SettingCollection(new SettingCollection{
            //     ImageResolutionEnhancement = IMG_RESENH.RE_STANDARD,
            //     ImageDeskew = IMG_DESKEW.DSK_NO,
            //     DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
            //     RMTradeoff = RMTRADEOFF.TO_ACCURATE,
            //     ImageRotation =  IMG_ROTATE.ROT_AUTO,
            //      }),
            // new SettingCollection(new SettingCollection
            // {
            //     ImageResolutionEnhancement = IMG_RESENH.RE_AUTO,
            //     ImageDeskew = IMG_DESKEW.DSK_NO,
            //     DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
            //     RMTradeoff = RMTRADEOFF.TO_ACCURATE,
            //     ImageRotation =  IMG_ROTATE.ROT_AUTO,
            // }),
            // new SettingCollection(new SettingCollection{
            //     ImageResolutionEnhancement = IMG_RESENH.RE_YES,
            //     ImageDeskew = IMG_DESKEW.DSK_NO,
            //     DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
            //     RMTradeoff = RMTRADEOFF.TO_ACCURATE,
            //     ImageRotation =  IMG_ROTATE.ROT_AUTO,
            //  }),
            // new SettingCollection(new SettingCollection{
            //     ImageConversionMode = IMG_CONVERSION.CNV_GLOBAL,
            //     ImageBrightness = 0,
            //     ImageThreshold = 0,
            //     ImageResolutionEnhancement = IMG_RESENH.RE_YES,
            //     ImageDeskew = IMG_DESKEW.DSK_NO,
            //     DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
            //     RMTradeoff = RMTRADEOFF.TO_ACCURATE,
            //     ImageRotation =  IMG_ROTATE.ROT_AUTO,
            //  }),
            // new SettingCollection(new SettingCollection{
            //     ImageConversionMode = IMG_CONVERSION.CNV_SET,
            //     ImageBrightness = 0,
            //     ImageThreshold = 120,
            //     ImageResolutionEnhancement = IMG_RESENH.RE_NO,
            //     DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
            //     RMTradeoff = RMTRADEOFF.TO_ACCURATE,
            //     ImageRotation =  IMG_ROTATE.ROT_AUTO,
            //  })
            //};
            //var res = ocr.ExtractXmlFromImage(@"C:\Temp\IMAGEN_1.jpg", @"C:\Temp\IMAGEN_2.jpg","Pasaporte");

            ////clsLog.ArchiveLog(res);
            //clsLog.BDLog(1, "Test", "Test", res);

            //if (res == "1")
            //{
            //    Console.WriteLine("OCR Deshabilitado");
            //}
            //if (res == "2")
            //{
            //    Console.WriteLine("Error en OCR");
            //}


        }


        public static void InitEngine()
        {
            const string csdk203 = @"C:\EnginePlusv20";
            Engine.SetLicenseKey(@"C:\EnginePlusv20\NuanceOmniPageCaptureSDK_20.0.lcxz", "AV-8p-Kyv-Rf");
            Engine.Init(null, null, true, csdk203);
        }

        public static void QuitEngine()
        {
            Engine.ForceQuit();
            GC.WaitForPendingFinalizers();
        }
    }
}