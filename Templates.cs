﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml.Linq;
using Com.BancoAzteca.OCR;
using Nuance.Omnipage.SampleCode.Types;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
namespace Nuance.Omnipage.SampleCode
{
    /// <summary>
    /// The GSK template matching class
    /// </summary>
    public static class TemplatesWithFilterPlus
    {

        private static object templateLock = new object();
        /// <summary>
        /// Method that matches templates
        /// </summary>
        /// <param name="template">
        /// The template.
        /// </param>
        /// <param name="file">
        /// The file to match the templates
        /// </param>
        /// <param name="settingCollection">
        /// Settings to be tested with the file
        /// </param>
        /// <param name="useJsonOverrides">
        ///To control if its needed to use the overrides in the json files
        /// </param>
        public static OcrPage MatchTemplates(string template, string file, SettingCollection settingCollection = null, bool useJsonOverrides = true)
        {
           // Console.WriteLine($"{file} on thread #{Thread.CurrentThread.ManagedThreadId}");
            using (SettingCollection settings = new SettingCollection(settingCollection))
            {
                using (var library = new FormTemplateLibrary(template, settings))
                {
                    // open source file as multi-page format
                    using (var page = new Page(file, 0, settings))
                    {
                        page.Preprocess();

                        //page.Recognize();

                        if (settingCollection != null)
                        {
                            page.Recognize();
                        }

                        var confidence = 0;
                        var matching = 0;
                        FormTemplateCollection collection = null;
                        MatchingFormTemplate matchingFormTemplate = null;

                        try
                        {
                            matchingFormTemplate =
                                page.FindFormTemplate(library, out collection, out confidence, out matching);

                            //var tepl
                        }
                        catch (CSDKException ex)
                        {
                            Com.BancoAzteca.OCR.clsLog.ArchiveLog(
                                "Falló en hacer match con template debido a: " + ex.Message);
                            Com.BancoAzteca.OCR.clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage",
                                "Falló en hacer match con template debido a: " + ex.Message);



                            //lock (typeof(string))
                            //{
                            //    Console.ForegroundColor = ConsoleColor.Red;
                            //    Console.WriteLine($"{ex.Code}: {ex.Message}");
                            //    Console.ForegroundColor = ConsoleColor.White;
                            //}
                            Console.WriteLine("Excepción al encontrar template" + ex);
                            if (ex.Code == RECERR.IMG_TEMPLATENOTMATCHED_ERR)
                            {
                                page.Preprocess();
                                page.Recognize();
                            }

                            if (ex.Code == RECERR.IMG_ANCHORNOTFOUND_ERR)
                            {
                                page.Preprocess();
                                page.Recognize();
                            }
                        }

                        if (matchingFormTemplate == null)
                        {

                            page.Preprocess();
                            page.Recognize();
                            if (page.GetPageLetters() != null && !string.IsNullOrEmpty(page.GetPageLetters()))
                            {
                                VotingCardOcr.generalPage = page.GetPageLetters();
                                VotingCardOcr.genPage = page;
                            }


                            Console.WriteLine(page.GetPageLetters());
                            return new OcrPage(page, null, file, 0)
                                {Confidence = 0, FormTemplateName = "No matching template"};
                        }

                        using (collection)
                        {
                            if (matching > 0)
                            {
                                try
                                {
                                    page.ApplyFormTemplate(matchingFormTemplate);
                                }
                                catch (CSDKException ex)
                                {
                                    Com.BancoAzteca.OCR.clsLog.ArchiveLog(
                                        "Falló en aplicar el template debido a: " + ex.Message);
                                    Com.BancoAzteca.OCR.clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage",
                                        "Falló en aplicar el template debido a: " + ex.Message);
                                    return new OcrPage(page, null, file, 0)
                                        {Confidence = matchingFormTemplate.Confidence, FormTemplateName = ex.Message};
                                }

                                if (useJsonOverrides)
                                {
                                    ApplyZoneOverrides(template, page, matchingFormTemplate);
                                }

                                page.Recognize();

                                var ocrPage = new OcrPage(page, matchingFormTemplate, file);

                                var filterPlusZones =
                                    GetEnabledZoneSettings(GetZoneTypesFile(template, matchingFormTemplate))
                                        .Where(z => z.ZoneFilter == CHR_FILTER.FILTER_PLUS &&
                                                    !string.IsNullOrEmpty(z.FilterPlus))
                                        .ToArray();

                                // re-ocr the filterplus zones (one by one)
                                foreach (var zone in page.UserZones.OfType<InputZone>()
                                    .Where(zonea => filterPlusZones.Any(f => f.Name.Equals(zonea.Name)))
                                    .Select(zonea => new
                                    {
                                        zonea.Name, ZoneInfo = zonea.GetZoneInfo(), Zone = zonea,
                                        FilterPlus = zonea.Attributes["FilterPlus"]
                                    })
                                    .ToArray())
                                {
                                    page.UserZones.DeleteAll();

                                    if (!string.IsNullOrEmpty(zone.FilterPlus))
                                    {
                                        settings.FilterPlus = zone.FilterPlus;
                                    }
                                    else
                                    {
                                        zone.ZoneInfo.filter = CHR_FILTER.FILTER_ALL;
                                    }

                                    page.UserZones.InsertInputZone(zone.ZoneInfo);

                                    page.Recognize();

                                    // update the zone text with the re-recognized one.
                                    OcrZone ocrZone = ocrPage.OcrZones.First(z =>
                                        z.Name.Equals(zone.Name, StringComparison.InvariantCultureIgnoreCase));

                                    ocrZone.Filter = CHR_FILTER.FILTER_PLUS;
                                    ocrZone.Text = page.RecognizedText().Trim();

                                    ocrZone.ZoneConfidence = page.OCRZones[0].GetZoneConfidence();



                                    ocrZone.RefreshZone();
                                }

                                return ocrPage;
                            }
                            else
                            {
                                Console.WriteLine(page.RecognizedText());
                                return new OcrPage(page, null, file, 0)
                                    {Confidence = 0, FormTemplateName = "No matching template"};
                            }
                        }
                    }

                }
            }
        }

        public static IEnumerable<OcrZone> ForceApplyTemplate(string file, string templateLibrary, string templateName)
        {
            using (var settings = new SettingCollection())
            {
                settings.Load(@".\settings\setting7.sts");
                using (var page = new Page(file, 0, settings))
                {
                    page.Preprocess();
                    page.LoadZones(Encoding.UTF8.GetBytes(GetZoneFileFromTemplate(templateLibrary, templateName)));

                    string jsonOverride = Path.Combine(Path.GetDirectoryName(templateLibrary), $"{templateName}.json");

                    if (File.Exists(jsonOverride))
                    {
                        ApplyZoneOverrides(page, jsonOverride);
                    }

                    page.Recognize();



                    foreach (OCRZone userZone in page.OCRZones)
                    {
                        yield return new OcrZone(userZone);
                    }
                }
            }
        }

        /// <summary>Gets the content of the zon file.</summary>
        /// <param name="templateFile"></param>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static string GetZoneFileFromTemplate(string templateFile, string templateName)
        {

           
        //    byte[] temparr = null;
           
    //        Encoding.Convert(Encoding.GetEncoding(437), Encoding.UTF8, temparr);
          //  Stream stream = new MemoryStream(temparr);
            using (var archive = Ionic.Zip.ZipFile.Read(templateFile))
            {
                var sessionDataStream =
                    
                    archive.Entries.FirstOrDefault(file => file.FileName.Equals($@"{templateName}.zon", StringComparison.InvariantCultureIgnoreCase)
                                                           || file.FileName.Equals($@"{templateName}.zon", StringComparison.InvariantCultureIgnoreCase));

                if (sessionDataStream != null)
                {
                    using (StreamReader reader = new StreamReader(sessionDataStream.OpenReader(), Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }

                throw new ArgumentException($"{templateName} template cannot be found in {templateFile}");
            }
        }

        private static void ApplyZoneOverrides(Page page, string zoneTypesFile)
        {
            var enabledZoneSettings = GetEnabledZoneSettings(zoneTypesFile);

            foreach (var zoneSetting in enabledZoneSettings)
            {
                var inputZone = page.UserZones.OfType<InputZone>().FirstOrDefault(z => z.Name.Equals(zoneSetting.Name));

                if (inputZone != null)
                {
                    var zoneInfo = inputZone.GetZoneInfo();

                    zoneInfo.type = zoneSetting.ZoneType;
                    zoneInfo.filter = zoneSetting.ZoneFilter;

                    inputZone.Update(zoneInfo);

                    if (!String.IsNullOrEmpty(zoneSetting.RegexPattern))
                    {
                        inputZone.SetAttribute("Regex", zoneSetting.RegexPattern);
                    }

                    if (!String.IsNullOrEmpty(zoneSetting.FilterPlus))
                    {
                        inputZone.SetAttribute("FilterPlus", zoneSetting.FilterPlus);
                    }
                }
            }
        }


        public static string LookForPattern(string file, string pattern, SettingCollection[] settingCollections)
        {
            return 
            settingCollections
                //.AsParallel()
                .Select(settingCollection =>
                {
                    using (var settings = new SettingCollection(settingCollection))
                    {
                        using (var page = new Page(file, 0, settings))
                        {
                            page.Preprocess();

                            page.Recognize();

                            //Console.WriteLine(page.RecognizedText());

                            var leftTextZoneText = page.OCRZones.Cast<OCRZone>().Where(z => z.GetZoneInfo().type == ZONETYPE.WT_LEFTTEXT)
                                                        .Select(z => z.GetZoneText())
                                                        .ToArray();
                           
                                #region Log Resultados Puro
                                OcrPage FullPageOcrPage = new OcrPage();
                                OcrZone[] fullPageOcrZones = new OcrZone[] {
                                    new OcrZone(){
                                        Name = "FullPageOcr",
                                        Text = leftTextZoneText.Select(i => i).DefaultIfEmpty().Aggregate((i, j) => i + " " + j)
                                    }
                                };
                                FullPageOcrPage.FormTemplateName = "FullPageOcr";
                                FullPageOcrPage.OcrZones = fullPageOcrZones;

                                OcrPage[] OcrResults = new OcrPage[] { FullPageOcrPage };

                                Types.ExtensionMethods.GetRawExtractionData(OcrResults, Constants.DocumentNames.CredencialIfe, "FullPageOcr");
                                #endregion
                            


                            //Console.WriteLine(string.Join("\n", leftTextZoneText));

                            return string.Join("\n", leftTextZoneText);
                        }
                    }
                }).Select(t => Regex.Matches(t, pattern).Cast<Match>().Select(m => m.Value)).SelectMany(c => c).FirstOrDefault();
        }


        public static string GetMrzFromPassportWhenTemplateNotMatched(SettingCollection settingCollection, string file) {
            using (var settings = new SettingCollection(settingCollection)) {
                using (var page = new Page(file, 0, settings))
                {
                    page.Preprocess();

                    page.Recognize();

                    string extractedText;

                    if (page.OCRZones.Count > 0)
                    {
                        extractedText = page.OCRZones[0].Text;
                    }
                    else {
                        extractedText = page.RecognizedText();
                    }

                    var mrzText = extractedText
                                        .Split('\n')
                                        .Select(line => line.Trim().Replace(" ", string.Empty))
                                        .Where(line => !string.IsNullOrEmpty(line))
                                        .Reverse()
                                        .Take(2)
                                        .Reverse()
                                        .ToArray();

                   
                        #region Log Resultados Puro
                        OcrPage PassportPage = new OcrPage();
                        OcrZone[] passportZones = new OcrZone[] {
                            new OcrZone(){
                                Name = "FullPageOcr",
                                Text = extractedText
                            }
                        };
                        PassportPage.FormTemplateName = "FullPageOcr";
                        PassportPage.OcrZones = passportZones;
                        PassportPage.Confidence = 100;
                        PassportPage.FileName = @"C:\IMAGEN_01.jpg";
                        OcrPage[] OcrResults = new OcrPage[] { PassportPage };

                        Types.ExtensionMethods.GetRawExtractionData(OcrResults, Constants.DocumentNames.Pasaporte, "FullPageOcr");
                        #endregion
                    



                    return string.Join("\n", mrzText);
                }
            }
            
        }

        public static string GetMrzFromPage(string file, SettingCollection settingCollection)
        {
                using (var settings = new SettingCollection(settingCollection))
                {
                    using (var page = new Page(file, 0, settings))
                    {
                        page.Preprocess();

                        page.Recognize();

                        var mrzText = page.RecognizedText()
                                            .Split('\n')
                                            .Select(line => line.Trim().Replace(" ", string.Empty))
                                            .Where(line => line.Length == 30)
                                            .Reverse()
                                            .Take(3)
                                            .Reverse()
                                            .ToArray();
                        
                            #region Log Resultados Puro

                            OcrPage PassportPage = new OcrPage();
                            OcrZone[] passportZones = new OcrZone[]
                            {
                                new OcrZone()
                                {
                                    Name = "FullPageOcr",
                                    Text = page.RecognizedText()
                                }
                            };
                            PassportPage.FormTemplateName = "FullPageOcr";
                            PassportPage.OcrZones = passportZones;

                            OcrPage[] OcrResults = new OcrPage[] {PassportPage};

                            Types.ExtensionMethods.GetRawExtractionData(OcrResults,
                                Constants.DocumentNames.CredencialIfe, "FullPageOcr");

                            #endregion
                        


                        return string.Join("\n", mrzText);
                    }
                }
        }

        private static void ApplyZoneOverrides(string template, Page page, MatchingFormTemplate matchingFormTemplate)
        {
            string zoneTypesFile = GetZoneTypesFile(template, matchingFormTemplate);

            if (!File.Exists(zoneTypesFile))
            {
                var zoneDefaults = page.UserZones
                                       .OfType<InputZone>()
                                       .Select(z => new ZoneSettings
                                       {
                                           Name = z.Name,
                                           ZoneFilter = z.GetZoneInfo().filter,
                                           ZoneType = z.GetZoneInfo().type
                                       })
                                       .ToArray();

                // create the template overrid file with default settings. You have to set the Enabled to true to apply the zone override !
                File.WriteAllText(zoneTypesFile, zoneDefaults.ToJson());
            }
            else
            {
                var enabledZoneSettings = GetEnabledZoneSettings(zoneTypesFile);

                foreach (var zoneSetting in enabledZoneSettings)
                {
                    var inputZone = page.UserZones.OfType<InputZone>().FirstOrDefault(z => z.Name.Equals(zoneSetting.Name));

                    if (inputZone != null)
                    {
                        var zoneInfo = inputZone.GetZoneInfo();

                        zoneInfo.type = zoneSetting.ZoneType;
                        zoneInfo.filter = zoneSetting.ZoneFilter;

                        inputZone.Update(zoneInfo);

                        if (!String.IsNullOrEmpty(zoneSetting.RegexPattern))
                        {
                            inputZone.SetAttribute("Regex", zoneSetting.RegexPattern);
                        }

                        if (!String.IsNullOrEmpty(zoneSetting.FilterPlus))
                        {
                            inputZone.SetAttribute("FilterPlus", zoneSetting.FilterPlus);
                        }
                    }
                }
            }
        }

        private static string GetZoneTypesFile(string template, MatchingFormTemplate matchingFormTemplate)
        {
            return Path.Combine(Path.GetDirectoryName(template) ?? throw new InvalidOperationException(), $"{matchingFormTemplate.Name}.json");
        }

        private static IEnumerable<ZoneSettings> GetEnabledZoneSettings(string zoneTypesFile)
        {
            return File.ReadAllText(zoneTypesFile).FromJson<ZoneSettings[]>().Where(z => z.Enabled);
        }

        internal static Page GetPage(string imageFile)
        {
            using (var settings = new SettingCollection())
            {
                using (var page = new Page(imageFile, 0, settings))
                {
                    page.Preprocess();
                    page.Recognize();
                    return page;
                }
            }
        }
    }
}
