﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Banco.PD3.Persistence;
using Banco.PD3.Persistence.Entities;
using Banco.PD3.Persistence.Enums;

namespace Nuance.Omnipage.SampleCode
{
    internal class DAO
    {
        private static SqlPersister CreatePD3Connection() {
            try {
                PD3Connection ConexionBaseDatos = new PD3Connection();
                String path = @"K:\NET64_BAZ\Certs\";
                String certificado = "baz-f1582e18-08a9-476b-af55-a03c3b977706.pd3";
                ConexionBaseDatos = Certificater.GetPD3Connection(path, certificado);
                SqlPersister persistence = new SqlPersister(ConexionBaseDatos);
                return persistence;
            }
            catch (Exception Ex) {
                throw Ex;
            }
        }

        internal static string GetEstadoName(string idEstado)
        {
            try
            {
                System.Data.SqlClient.SqlDataReader ObjDataReader = CreatePD3Connection().ScriptToDataReader("SELECT Est.fcNombre AS NombreEstado FROM TCCRDigEstadosOCR Est WITH(NOLOCK) WHERE Est.fiEstadoId = " + idEstado);

                if (ObjDataReader.HasRows)
                {
                    ObjDataReader.Read();

                    return ObjDataReader["NombreEstado"].ToString().ToUpper().Trim();
                }
                else
                    return idEstado;
            }
            catch (Exception Ex)
            {
                return idEstado;
            }
        }

        internal static string GetEstadoAbrev(string idEstado)
        {
            try
            {
                System.Data.SqlClient.SqlDataReader ObjDataReader = CreatePD3Connection().ScriptToDataReader("SELECT Est.fcAbrev AS AbrevEstado FROM TCCRDigEstadosOCR Est WITH(NOLOCK) WHERE Est.fiEstadoId = " + idEstado);

                if (ObjDataReader.HasRows)
                {
                    ObjDataReader.Read();

                    return ObjDataReader["AbrevEstado"].ToString().ToUpper().Trim();
                }
                else
                    return idEstado;
            }
            catch (Exception Ex)
            {
                return idEstado;
            }
        }

        internal static string GetMunicipioName(string idEstado, string idMunicipio)
        {
            try
            {
                System.Data.SqlClient.SqlDataReader ObjDataReader = CreatePD3Connection().ScriptToDataReader("SELECT Est.fcNombre AS NombreEstado, Muni.fcNombre AS NombreMunicipio FROM TCCRDigEstadosOCR Est WITH(NOLOCK) INNER JOIN TCCRDigMunicipiosOCR Muni WITH(NOLOCK) ON Est.fiEstadoId = Muni.fiEstadoId WHERE Est.fiEstadoId = " + idEstado + " AND Muni.fiClaveId = " + idMunicipio);

                if (ObjDataReader.HasRows)
                {
                    ObjDataReader.Read();

                    return ObjDataReader["NombreMunicipio"].ToString().ToUpper().Trim();
                }
                else
                    return idMunicipio;
            }
            catch (Exception Ex)
            {
                return idMunicipio;
            }
        }
    }
}
