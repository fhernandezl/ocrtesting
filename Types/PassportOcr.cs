﻿using System;
using System.Linq;
using System.Xml.Serialization;
using Nuance.OmniPage.CSDK.Objects;
using Com.BancoAzteca.OCR;
using System.Collections.Generic;
using Nuance.OmniPage.CSDK.ArgTypes;
using CrRNRBAZDIGOCRPostProcessing.Models;
using CrRNRBAZDIGOCRPostProcessing;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class PassportOcr : IdentityDocumentOcr
    {
        public string[] Images { get; }
        [XmlIgnore]
        public string[] Templates { get; }
        [XmlIgnore]
        public SettingCollection[] SettingProfiles { get; set; }
        [XmlIgnore]
        public OcrPage[] OcrResults { get; private set; }
        public Xml.Page paginaXml { get; set; }

        public string Nombre { get; set; }
        public string Folio { get; set; }
        public string Pais { get; set; }
        public double Weight { get; set; }
        public int ConfidenceLevel { get; set; }

        public PassportOcr(string[] images, string[] templates)
        {
            Images = images;
            Templates = templates;
        }

        public override void DisplayData(IdentityDocumentBase passport)
        {
            if (!(passport is Passport passportData))
            {
                throw new ArgumentException($"{nameof(passport)} must be of type Passport");
            }

            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("MRZ lines:");
            Console.WriteLine($"{passportData.MrzLines[0]}");
            Console.WriteLine($"{passportData.MrzLines[1]}\n");

            Console.WriteLine($"      Name: {passportData.Surname}, {string.Join(" ", passport.GivenNames)}");
            Console.WriteLine($"    Gender: {passportData.Sex}");
            Console.WriteLine($"   Country: {passportData.CountryCode}");
            Console.WriteLine($"       DOB: {passportData.Dob:yyyy-MM-dd}");
            Console.WriteLine($"  Passport: {passportData.DocumentNumber}");
            Console.WriteLine($"     Valid: {passportData.ExpirationDate:yyyy-MM-dd}");
            Console.WriteLine($" Personal#: {passportData.PersonalNumber.Trim()}");

            Console.WriteLine("\nCheck digits (readed - calculated)");
            Console.WriteLine($"   Passport: {passportData.DocumentNumberCheckDigit} - {passportData.DocumentNumberCheckDigitCalculated}");
            Console.WriteLine($"     Expiry: {passportData.ExpirationDateCheckDigit} - {passportData.ExpirationDateCheckDigitCalculated}");
            Console.WriteLine($"Personal id: {passportData.PersonalNumberCheckDigit} - {passportData.PersonalNumberCheckDigitCalculated}");
            Console.WriteLine($"   Checksum: {passportData.CheckDigit} - {passportData.CheckDigitCalculated}");
            Console.WriteLine($"   Accuracy: {passportData.CheckWeight * 100.0 / 4}%");

            Console.ForegroundColor = ConsoleColor.White;
        }
        public OcrPage BackPage { get; set; } = new OcrPage();

        private IEnumerable<OcrPackage> GetOcrPackages()
        {
            return from setting in SettingProfiles
                   from file in Images
                   from template in Templates
                   select new OcrPackage
                   {
                       FileName = file,
                       Settings = setting,
                       Template = template
                   };
        }

        public void Process()
        {
            OcrResults = GetOcrPackages()//.AsParallel().WithDegreeOfParallelism(Program.Paralellismlevel)
                .Select(ocr => TemplatesWithFilterPlus.MatchTemplates(ocr.Template, ocr.FileName, ocr.Settings))
                .ToArray();
            OCRProcessor.matchingtemplate = OcrResults.Any(p => p.FormTemplateName != "No matching template");
        }

        /// <summary>
        ///OCR Extraction is done here
        /// </summary>
        public void ExtractData()
        {
            var lastPageResults = OcrResults.AsParallel().WithDegreeOfParallelism(Program.Paralellismlevel).OrderByDescending(BackPageWeight).ThenByDescending(p => p.Confidence).FirstOrDefault();

            //paginaXml = new CrRNRBAZDIGOCRPostProcessing.Models.Xml.Page();

            if (lastPageResults != null)
            {
                //paginaXml.DatosEngineOcr.OcrZones = lastPageResults.OcrZones.Select(z => new Xml.OcrDataZone() { Name = z.Name, ZoneConfidence = z.ZoneConfidence.ToString(), ZoneType = z.ZoneType.ToString(), BoundingRect = z.BoundingRect.ToString(), FillingMethod = z.FillingMethod.ToString(), Filter = "Filtro: " + z.Filter.ToString() + " FilterPlus: " + z.FilterPlus, IsMatch = z.IsMatch.ToString(), RecognitionModule = z.RecognitionModule.ToString(), RegexPattern = z.RegexPattern }).ToList();
                if (lastPageResults.OcrZones.Any(z => z.Name.Equals("MRZ", StringComparison.InvariantCultureIgnoreCase)))
                {
                    var mrzZone = lastPageResults.OcrZones.First(z => z.Name.Equals("MRZ", StringComparison.InvariantCultureIgnoreCase));
                    var issuingDate = lastPageResults.OcrZones.First(z => z.Name.Equals("IssuingDate", StringComparison.InvariantCultureIgnoreCase));

                    paginaXml = FillXmlPageWithExtractedData(mrzZone?.Text, issuingDate?.Text);
                }
                
                BackPage = lastPageResults;
            }

            Weight = BackPageWeight(BackPage);
            var idZones = BackPage.OcrZones.ToList();
            ConfidenceLevel = (int)(idZones.Sum(z => z.ZoneConfidence) / idZones.Count());

            if (OcrResults.All(p => p.FormTemplateName.Equals("No matching template")) && OCRProcessor.matchingtemplate != true)
            {
                OCRProcessor.matchingtemplate = false;
            }
        }

        private Xml.Page FillXmlPageWithExtractedData(string mrz, string issuingDate) {

            if (string.IsNullOrEmpty(mrz)) return null;

            if (Passport.IsMrz(mrz.Trim()))
            {
                Passport datosPasaporte = new Passport(mrz, true, false);

                //Xml.Page paginaXml = new Xml.Page();

                string[] nombreCompleto = new string[] { datosPasaporte.Surname, datosPasaporte.GivenNames.ToList().Select(i => i).Aggregate((i, j) => i + " " + j)};
                paginaXml = XmlResult.GeneratePassportPage(string.Join("\n", nombreCompleto),
                    datosPasaporte.DocumentNumber, IdentityDocumentBase.GetCountryByCode(datosPasaporte.Nationality),
                    IdentityDocumentBase.GetCountryByCode(datosPasaporte.CountryCode),
                    datosPasaporte.Dob.ToString("dd/MM/yyyy"), issuingDate,
                    datosPasaporte.ExpirationDate.ToString("dd/MM/yyyy"), datosPasaporte.Sex, OCRProcessor.shouldlog,
                    OCRProcessor.shouldlogtodb, OCRProcessor.typeofpostprocessing);

                return paginaXml;
            }

            return null;
        }

        private static double BackPageWeight(OcrPage page)
        {
            Console.WriteLine(page.FormTemplateName);
            //GetZone(page, "Nombre").Text.Contains("?")

            if (!string.IsNullOrEmpty(GetZone(page, "IssuingDate").Text))
            {
                GetZone(page, "IssuingDate").ZoneConfidence = GetZone(page, "IssuingDate").ZoneConfidence * .1;
            }
            if (GetZone(page, "MRZ").Text.Split('\n').Count() != 3 ||
                GetZone(page, "MRZ").Text.Split('\n').Where(x => x.Length == 44).Count() != 3)
            {
                GetZone(page, "MRZ").ZoneConfidence = GetZone(page, "MRZ").ZoneConfidence * .1;
            }

            return GetZone(page, "MRZ").ZoneConfidence * 8 +
                   (GetZone(page, "IssuingDate").ZoneConfidence) * 2;
        }

        private static OcrZone GetZone(OcrPage page, string zoneName)
        {
            return page.OcrZones.FirstOrDefault(z => z.Name.Equals(zoneName, StringComparison.InvariantCultureIgnoreCase)) ?? new OcrZone();
        }

        public override string GetMrzLines(string imagePath)
        {
            Engine.Init(null, null, true, @"c:\Program Files\Nuance\OPCaptureSDK20\Bin64");

            var recognizedText = GetRecognizedText(imagePath, true);

            var micrLines = recognizedText
                .Select(text => text.Split('\n').Select(line => line.Trim().Replace(" ", string.Empty)).Where(line => new[] { 44 }.Contains(line.Length)).Reverse().Take(2).Reverse().ToArray())
                .Where(lines => lines.Length == 2)
                .Select(x => new { Lines = x, PassportData = new Passport(string.Join("\n", x), true) })
                .OrderByDescending(x => x.PassportData.CheckWeight)
                .ToArray();

            return micrLines.Select(x => string.Join("\n", x.Lines)).FirstOrDefault();
        }

        public void ValidateScannedDocument(string idtype)
        {
            List<KeyValuePair<string,string[]>> kvpidtemplate = new List<KeyValuePair<string, string[]>>();

            kvpidtemplate.Add(new KeyValuePair<string, string[]>("Pasaporte", new string[] { "Type1_Passport", "Type2_Passport", "Type3_Passport" , "Type4_Passport" }));

            if (!(kvpidtemplate.Find(z => z.Key == idtype).Value.Any(x => x.Equals(BackPage.FormTemplateName ?? BackPage.FormTemplateName ?? ""))))
            {
                OCRProcessor.matchingdocument = false;
            }
            else
            {
                OCRProcessor.matchingdocument = true;
            }
        }

        public void ExtractDataFromFullPageOCR()
        {
            string mrzLinesFromFullOcrPage = TemplatesWithFilterPlus.GetMrzFromPassportWhenTemplateNotMatched(new SettingCollection(), @"C:\Temp\IMAGEN_1.jpg");

            paginaXml = FillXmlPageWithExtractedData(mrzLinesFromFullOcrPage, null);
        }
    }
}
