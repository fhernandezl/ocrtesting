﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using OmniPage.CSDK.ArgTypes;
    using OmniPage.CSDK.Objects;

    /// <summary>
    /// The passport ocr.
    /// </summary>
    public abstract class IdentityDocumentOcr
    {
        protected readonly SettingCollection[] OcrSettings;
                
        public IdentityDocumentOcr()
        {
            Engine.Init(null, null, true, @"c:\Program Files\Nuance\OPCaptureSDK20\Bin64");

            OcrSettings = new[] {
                new SettingCollection(
                    new SettingCollection
                    {
                        DefaultFillingMethod = FILLINGMETHOD.FM_OMNIFONT,
                        DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                        RMTradeoff = RMTRADEOFF.TO_BALANCED,
                        ImageDeskew = IMG_DESKEW.DSK_AUTO3D,
                        DefaultFilter = CHR_FILTER.FILTER_PLUS,
                        FilterPlus = "0123456789<ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                        PageDescription = PAGEDESCRIPTION.LZ_GRAPHICS_NO | PAGEDESCRIPTION.LZ_TABLE_ONE
                    }),
                    new SettingCollection(
                        new SettingCollection
                        {
                            DefaultFillingMethod = FILLINGMETHOD.FM_OMNIFONT,
                            DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                            RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                            ImageDeskew = IMG_DESKEW.DSK_AUTO3D,
                            DefaultFilter = CHR_FILTER.FILTER_PLUS,
                            FilterPlus = "0123456789<ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                            ImageResolutionEnhancement = IMG_RESENH.RE_YES,                            
                            PageDescription = PAGEDESCRIPTION.LZ_GRAPHICS_NO | PAGEDESCRIPTION.LZ_TABLE_ONE
                        }),
                new SettingCollection(
                    new SettingCollection
                    {
                        DefaultFillingMethod = FILLINGMETHOD.FM_OMNIFONT,
                        DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                        RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                        ImageDeskew = IMG_DESKEW.DSK_AUTO3D,
                        DefaultFilter = CHR_FILTER.FILTER_PLUS,
                        FilterPlus = "0123456789<ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                        ImageDownsample =  true,
                        ImageResolutionEnhancement = IMG_RESENH.RE_NO,
                        PageDescription = PAGEDESCRIPTION.LZ_GRAPHICS_NO | PAGEDESCRIPTION.LZ_TABLE_ONE
                    }),
                    new SettingCollection(
                       new SettingCollection
                       {
                           DefaultFillingMethod = FILLINGMETHOD.FM_OCRB,
                           DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_MTX,
                           RMTradeoff = RMTRADEOFF.TO_FAST,
                           ImageDeskew = IMG_DESKEW.DSK_2D,
                           DefaultFilter = CHR_FILTER.FILTER_PLUS,
                           FilterPlus = "0123456789<ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                           PageDescription = PAGEDESCRIPTION.LZ_GRAPHICS_NO | PAGEDESCRIPTION.LZ_TABLE_ONE
                       }),
                    new SettingCollection(
                        new SettingCollection
                        {
                            DefaultFillingMethod = FILLINGMETHOD.FM_OCRB,
                            DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_MTX,
                            RMTradeoff = RMTRADEOFF.TO_BALANCED,
                            ImageDeskew = IMG_DESKEW.DSK_AUTO,
                            DefaultFilter = CHR_FILTER.FILTER_PLUS,
                            FilterPlus = "0123456789<ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                        })
                };
        }

        protected string[] GetRecognizedText(string imagePath, bool showBenchmarkData)
        {           
            Stopwatch totalStopWatch = Stopwatch.StartNew();

            string[] recognizedText = OcrSettings
                //.AsParallel()
                //.WithCancellation(new CancellationTokenSource().Token)
                .Select(setting =>
                {
                    var sw = Stopwatch.StartNew();

                    try
                    {
                        setting.SpellLanguages.Spell = false;
                        setting.Languages.Manage(MANAGE_LANG.SET_LANG, LANGUAGES.LANG_NO);

                        using (var page = new Page(imagePath, 0, setting))
                        {
                            page.Preprocess();
                            page.Recognize();
                            return page.RecognizedText();
                        }
                    }
                    catch (Exception ex)
                    {
                        Com.BancoAzteca.OCR.clsLog.ArchiveLog("Falló en aplicar preprocesamiento o reconocimiento debido a: " + ex.Message);
                        Com.BancoAzteca.OCR.clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", "Falló en aplicar preprocesamiento o reconocimiento debido a: " + ex.Message);
                        return ex.Message;
                    }
                    finally
                    {
                        sw.Stop();

                        if (showBenchmarkData)
                        {
                            Console.Write($"#{Thread.CurrentThread.ManagedThreadId:D3}: {sw.ElapsedMilliseconds} msec, ");
                        }
                    }
                }).ToArray();
           
            totalStopWatch.Stop();

            if (showBenchmarkData)
            {
                Console.WriteLine($"Total ocr time: {totalStopWatch.ElapsedMilliseconds} msec");
            }

            return recognizedText;
        }

        public abstract void DisplayData(IdentityDocumentBase passport);

        public abstract string GetMrzLines(string imagePath);
    }
}
