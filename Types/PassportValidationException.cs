﻿using System;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class PassportValidationException : Exception
    {
        public PassportValidationException(string errorMessage) : base(errorMessage)
        {

        }
    }
}
