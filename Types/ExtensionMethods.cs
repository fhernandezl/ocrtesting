﻿using System.Linq;
using Com.BancoAzteca.OCR;

namespace Nuance.Omnipage.SampleCode.Types
{
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    public static class ExtensionMethods
    {
        /// <summary>
        /// Serializes the object to an Xml file
        /// </summary>
        /// <param name="obj">the object to be saved</param>
        /// <param name="encoding">the encoding used in the XmlSerialization.</param>
        /// <returns>the serialized object in Xml.</returns>
        public static string ToXml(this object obj, Encoding encoding)
        {
            var serializer = new XmlSerializer(obj.GetType());
            using (var stringWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings { Encoding = encoding, Indent = true }))
                {
                    serializer.Serialize(writer, obj);

                    writer.Flush();
                    return stringWriter.ToString();
                }
            }
        }

        /// <summary>
        /// returns the object as serialized Xml
        /// </summary>
        /// <param name="obj">the object to be serialized.</param>
        /// <returns>an Xml serialized object.</returns>
        public static string ToXml(this object obj)
        {
            return obj.ToXml(Encoding.UTF8);
        }

        /// <summary>
        /// Deserializes the fileName into a T type
        /// </summary>
        /// <typeparam name="T">the type to be used during deserialization</typeparam>
        /// <param name="fileName">the full-path filename of the .Xml</param>
        /// <returns>an instance of T</returns>
        public static T Deserialize<T>(this string fileName)
        {
            T t;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(fileName))
            {
                t = (T)serializer.Deserialize(tr);
                tr.Close();
            }

            return t;
        }

        public static void GetRawExtractionData(OcrPage[] OcrResults, string IdType, string TemplateMatchedName) {

            if (OCRProcessor.shouldlog)
            {
                Com.BancoAzteca.OCR.Types.Models.TemplateDataForBd ObjForLog = new Com.BancoAzteca.OCR.Types.Models.TemplateDataForBd(IdType);

                foreach (var result in OcrResults)
                {
                    ObjForLog.Confidence = string.Concat(ObjForLog.Confidence, "|", result.Confidence);
                    ObjForLog.Name = string.Concat(ObjForLog.Name, "|", result.FormTemplateName);
                    ObjForLog.MatchedFileName = string.Concat(ObjForLog.MatchedFileName, "|", result.FileName);
                    ObjForLog.IsMatched = !ObjForLog.IsMatched.Contains("true") && result.FormTemplateName == TemplateMatchedName ? string.Concat(ObjForLog.IsMatched, "true|") : string.Concat(ObjForLog.IsMatched, "false|");

                    foreach (var zone in result.OcrZones)
                    {
                        int currentZoneIndex = ObjForLog.Zones.FindIndex(z => z.Name.Equals(zone.Name));

                        if (currentZoneIndex != -1)
                        {
                            ObjForLog.Zones[currentZoneIndex].Value = string.Concat(ObjForLog.Zones[currentZoneIndex].Value, "|", zone.Text);
                            ObjForLog.Zones[currentZoneIndex].Confidence = string.Concat(ObjForLog.Zones[currentZoneIndex].Confidence, "|", zone.ZoneConfidence);
                        }
                    }
                }

                clsLog.ArchiveLog(string.Concat("Nombre de la imagen: ", ObjForLog.MatchedFileName.TrimStart('|').TrimEnd('|')));
                clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Nombre de la imagen: ", ObjForLog.MatchedFileName.TrimStart('|').TrimEnd('|')));

                clsLog.ArchiveLog(string.Concat("Nombre del template: ", ObjForLog.Name.TrimStart('|').TrimEnd('|')));
                clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Nombre del template: ", ObjForLog.Name.TrimStart('|').TrimEnd('|')));

                clsLog.ArchiveLog(string.Concat("Template seleccionado: ", ObjForLog.IsMatched.TrimStart('|').TrimEnd('|')));
                clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Template seleccionado: ", ObjForLog.IsMatched.TrimStart('|').TrimEnd('|')));

                clsLog.ArchiveLog(string.Concat("Confianza: ", ObjForLog.Confidence.TrimStart('|').TrimEnd('|')));
                clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Confianza: ", ObjForLog.Confidence.TrimStart('|').TrimEnd('|')));

                foreach (Com.BancoAzteca.OCR.Types.Models.ZoneDataForBd zone in ObjForLog.Zones)
                {
                    if (!string.IsNullOrEmpty(zone.Value))
                    {
                        int Counter = 0;
                        clsLog.ArchiveLog(string.Concat(zone.Name, ": ", zone.Value.TrimStart('|').TrimEnd('|')));

                        string lineStarter = string.Concat(zone.Name, ": ");

                        if ((lineStarter.Length + zone.Value.Length) > 255)
                        {
                            string cleanedZoneValue = zone.Value.TrimStart('|').TrimEnd('|');
                            lineStarter = string.Concat("Pte. ", Counter, " ", zone.Name, ": ");
                            string zoneValueWithHeadliner = string.Concat(lineStarter, cleanedZoneValue);

                            for (int i = 0; i < cleanedZoneValue.Length; i += (255 - lineStarter.Length))
                            {
                                int MaxIterationLength = 255 > cleanedZoneValue.Length - i ? cleanedZoneValue.Length - i : 255 - lineStarter.Length;
                                clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Pte. ", Counter + 1, " ", zone.Name, ": ", cleanedZoneValue.Substring(i, MaxIterationLength)));
                                Counter++;
                            }
                        }
                        else
                        {
                            clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat(zone.Name, ": ", zone.Value.TrimStart('|').TrimEnd('|')));
                        }


                        clsLog.ArchiveLog(string.Concat("Conf. ", zone.Name, ": ", zone.Confidence.TrimStart('|').TrimEnd('|')));
                        clsLog.BDLog(0, "ExtensionMethods", "GetRawExtractionData", string.Concat("Conf. ", zone.Name, ": ", zone.Confidence.TrimStart('|').TrimEnd('|')));
                    }
                }
            }
            
        }
    }
}
