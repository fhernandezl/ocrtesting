﻿using Newtonsoft.Json;

namespace Nuance.Omnipage.SampleCode.Types
{
    using System.Xml;

    using System.Xml.Serialization;

    using System;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// The passport.
    /// </summary>
    public class Passport : IdentityDocumentBase
    {
        public string Nationality { get; set; }

        public Passport()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Passport"/> class.
        /// </summary>
        /// <param name="mrpLines">
        /// The mrp lines.
        /// </param>
        /// <param name="tryCorrections">
        /// Tries to fix the numberic segments, for example O => 0, I => 1, l => 1
        /// </param>
        /// <param name="debugToConsole">
        /// if true then writes debug info to the Console.
        /// </param>
        public Passport(string mrpLines, bool tryCorrections = false, bool debugToConsole = false) : base(mrpLines, 2)
        {
            try
            {
                CountryCode = MrzLines[0].Substring(2, 3);

                string name = MrzLines[0].Substring(5);
                int nameIndex = name.IndexOf("<<", StringComparison.Ordinal);

                Surname = name.Substring(0, nameIndex).Replace('<', ' ');
                GivenNames = name.Substring(nameIndex + 2).Split(new[] {'<'}, StringSplitOptions.RemoveEmptyEntries);

                DocumentNumber = MrzLines[1].Substring(0, 9).Replace("<", String.Empty);
                DocumentNumberCheckDigit = FixDigits(MrzLines[1].Substring(9, 1));
                DocumentNumberCheckDigitCalculated = InvalidCharactersFound ? "_" : new String(GetCheckDigit(DocumentNumber), 1);

                PersonalNumber = FixDigits(MrzLines[1].Substring(28, 14)).Replace('<', ' ').Trim();
                PersonalNumberCheckDigit = new String(new[] {FixDigits(MrzLines[1].Substring(42, 1))[0]}).Replace('<', '0')[0];
                Sex = MrzLines[1].Substring(20, 1);
                Dob = DateTime.ParseExact(FixDigits(MrzLines[1].Substring(13, 6)), "yyMMdd",
                    CultureInfo.InvariantCulture);
                Nationality = MrzLines[1].Substring(10, 3);
                ExpiryDate = FixDigits(MrzLines[1].Substring(21, 6));

                ExpirationDate = DateTime.ParseExact(ExpiryDate, "yyMMdd", CultureInfo.InvariantCulture);
                CheckDigits = new[]
                {
                    new CheckDigit
                    {
                        Name = "DocumentNumber",
                        Value = DocumentNumberCheckDigit,
                        Calculated = DocumentNumberCheckDigitCalculated
                    },
                    new CheckDigit
                    {
                        Name = "PersonalNumber",
                        Value = new String(PersonalNumberCheckDigit, 1),
                        Calculated = new String(PersonalNumberCheckDigitCalculated, 1)
                    },
                    new CheckDigit
                    {
                        Name = "ExpirationDate",
                        Value = new String(ExpirationDateCheckDigit, 1),
                        Calculated = new String(ExpirationDateCheckDigitCalculated, 1)
                    },
                    new CheckDigit
                    {
                        Name = "CheckDigit",
                        Value = new String(CheckDigit, 1),
                        Calculated = new String(CheckDigitCalculated, 1)
                    }
                };
            }
            catch (Exception ex)
            {
                Com.BancoAzteca.OCR.clsLog.ArchiveLog("Falló en inicializar objeto pasaporte debido a: " + ex.Message);
                Com.BancoAzteca.OCR.clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", "Falló en inicializar objeto pasaporte debido a: " + ex.Message);
                if (debugToConsole)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }


        public static bool IsMrz(string text)
        {
            return (text ?? string.Empty).Replace(" ", string.Empty).Split('\n').Select(line => line.Trim()).Count(line => line.Length >= 42 && line.Length <= 44) == 2;
            //.All(line => line.Length >= 42 && line.Length <= 44);
        }

        public static void Display(Passport passportData)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("MRZ lines:");
            Console.WriteLine($"{passportData.MrzLines[0]}");
            Console.WriteLine($"{passportData.MrzLines[1]}\n");

            Console.WriteLine($"      Name: {passportData.Surname}, {string.Join(" ", passportData.GivenNames)}");
            Console.WriteLine($"    Gender: {passportData.Sex}");
            Console.WriteLine($"   Country: {passportData.CountryCode}");
            Console.WriteLine($"       DOB: {passportData.Dob:yyyy-MM-dd}");
            Console.WriteLine($"  Passport: {passportData.DocumentNumber}");
            Console.WriteLine($"     Valid: {passportData.ExpirationDate:yyyy-MM-dd}");
            Console.WriteLine($" Personal#: {passportData.PersonalNumber.Trim()}");

            Console.WriteLine("\nCheck digits (readed - calculated)");
            Console.WriteLine($"   Passport: {passportData.DocumentNumberCheckDigit} - {passportData.DocumentNumberCheckDigitCalculated}");
            Console.WriteLine($"     Expiry: {passportData.ExpirationDateCheckDigit} - {passportData.ExpirationDateCheckDigitCalculated}");
            Console.WriteLine($"Personal id: {passportData.PersonalNumberCheckDigit} - {passportData.PersonalNumberCheckDigitCalculated}");
            Console.WriteLine($"   Checksum: {passportData.CheckDigit} - {passportData.CheckDigitCalculated}");
            Console.WriteLine($"   Accuracy: {passportData.CheckWeight * 100.0 / 4}%");

            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// The is passport.
        /// </summary>
        [JsonIgnore]
        public bool IsPassport => MrzLines[0].StartsWith("P", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// The expiration date check digit.
        /// </summary>
        [JsonIgnore]
        public char ExpirationDateCheckDigit => FixDigits(MrzLines[1].Substring(27, 1))[0];

        /// <summary>
        /// The expiration date check digit calculated.
        /// </summary>
        [JsonIgnore]        
        public char ExpirationDateCheckDigitCalculated => InvalidCharactersFound ? '_' : GetCheckDigit(ExpiryDate);

        /// <summary>
        /// Personal number (may be used by the issuing country as it desires)
        /// Positions 29–42, Length 14
        /// </summary>        
        public string PersonalNumber { get; set; }

        /// <summary>
        /// The personal number check digit. Check digit over digits 29–42 (may be &lt; if all characters are &lt;)
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char PersonalNumberCheckDigit { get; set; }

        /// <summary>
        /// The personal number check digit calculated.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char PersonalNumberCheckDigitCalculated =>
            InvalidCharactersFound ? '_' : GetCheckDigit(FixDigits(MrzLines[1].Substring(28, 14)));

        /// <summary>
        /// Check digit over digits 1–10, 14–20, and 22–43
        /// </summary>
        public char CheckDigit => FixDigits(MrzLines[1].Substring(43, 1))[0];

        /// <summary>
        /// The check digit calculated.
        /// </summary>
        [JsonIgnore]
        public char CheckDigitCalculated =>
            InvalidCharactersFound
                ? '_'
                : GetCheckDigit(
                    string.Join(
                        string.Empty,
                        $"{MrzLines[1].Substring(0, 10)}{FixDigits(MrzLines[1].Substring(13, 7))}",
                        $"{FixDigits(MrzLines[1].Substring(21, 6))}{MrzLines[1][27]}{FixDigits(MrzLines[1].Substring(28, 14))}{MrzLines[1][42]}"));

        /// <summary>
        /// Calculates the 'weight' of the checksums. This will be used to pick the best recognition.
        /// </summary>
        [JsonIgnore]
        public int CheckWeight =>
            (DocumentNumberCheckDigit.Equals(DocumentNumberCheckDigitCalculated) ? 1 : 0)
            + (ExpirationDateCheckDigit == ExpirationDateCheckDigitCalculated ? 1 : 0)
            + (PersonalNumberCheckDigit == PersonalNumberCheckDigitCalculated ? 1 : 0)
            + (CheckDigit == CheckDigitCalculated ? 1 : 0);

        /// <summary>
        /// Gets or sets the expiry date.
        /// </summary>
        private string ExpiryDate { get; set; }

        /// <summary>
        /// Validates the passport MRZ text
        /// </summary>        
        public void Validate()
        {
            var invalidCharacters = MrzLines[0].Except(Constants.DigitValues.Keys).ToArray();

            if (invalidCharacters.Any())
            {
                throw new PassportValidationException(
                    $"Invalid characters found: {string.Join(",", invalidCharacters)}");
            }

            if (ExpirationDateCheckDigit != ExpirationDateCheckDigitCalculated)
            {
                throw new PassportValidationException($"Expiration date check digit\nDate: {ExpiryDate}");
            }
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The class ToString
        /// </returns>
        public override string ToString()
        {
            return
                $"{Surname}, {string.Join(" ", GivenNames)} ({Sex})\nCountry: {CountryCode}\nDate of birth: {Dob:yyyy-MM-dd}\nPassport number: {DocumentNumber}\nExpiry date: {ExpirationDate:yyyy-MM-dd}\nPersonal no: {PersonalNumber}";
        }        

        public void WriteXml(XmlWriter writer)
        {
            var properties = GetType().GetProperties();

            foreach (var propertyInfo in properties)
            {
                if (propertyInfo.IsDefined(typeof(XmlCommentAttribute), false))
                {
                    writer.WriteComment(propertyInfo.GetCustomAttributes(typeof(XmlCommentAttribute), false).Cast<XmlCommentAttribute>().Single().Value);
                }

                writer.WriteElementString(propertyInfo.Name, propertyInfo.GetValue(this, null).ToString());
            }
        }
    }
}
