﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System.Xml.Serialization;

    public class CheckDigit
    {

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("calculated")]
        public string Calculated { get; set; }
    }
}
