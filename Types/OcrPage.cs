﻿using CrRNRBAZDIGOCRPostProcessing.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using System;
using System.Web;
using System.Linq;
using System.Xml.Serialization;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class OcrPage
    {
        private Page genPage;
        private string v1;
        private int v2;
        private object imageFile;

        public OcrPage()
        {
        }

        public OcrPage(Page page, MatchingFormTemplate matchingFormTemplate, string fileName, int pageIndex = 1)
        {
            FileName = fileName;
            PageIndex = pageIndex;
            FormTemplateName = HttpUtility.UrlDecode(page.FormTemplateName);
            Languages = page.Languages;
            if (matchingFormTemplate != null)
            {
                Confidence = matchingFormTemplate.Confidence;
            }
            Dpi = page[IMAGEINDEX.II_CURRENT].ImageInfo.DPI;
            BitsPerPixel = page[IMAGEINDEX.II_CURRENT].ImageInfo.BitsPerPixel;
            Size = page[IMAGEINDEX.II_CURRENT].ImageInfo.Size;
            PreprocessInfo = page.PreprocessInfo;
            OcrZones = page.OCRZones.Cast<OCRZone>().Select(zone => new OcrZone(zone)).ToArray();
         
        }

        public OcrPage(Page genPage, string v1, int v2, object imageFile)
        {
            this.genPage = genPage;
            this.v1 = v1;
            this.v2 = v2;
            this.imageFile = imageFile;
        }

        public OcrPage(Page page, String formTemplateName, double confidence, string fileName, int pageIndex = 1)
        {
            FileName = fileName;
            PageIndex = pageIndex;
            FormTemplateName = formTemplateName;
            Languages = new LANGUAGES[]{LANGUAGES.LANG_SPA};
            Confidence = confidence;
            Dpi = new SIZE(0,0);
            BitsPerPixel = new ushort();
            Size = new SIZE(0,0);
            PreprocessInfo = new PREPROC_INFO();
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the applied form template name.
        /// </summary>
        [XmlAttribute]
        public string FormTemplateName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public double Confidence { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        public SIZE Dpi { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public ushort BitsPerPixel { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        public SIZE Size { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PREPROC_INFO PreprocessInfo { get; set; }

        /// <summary>
        /// Gets or sets the languages.
        /// </summary>
        [JsonProperty("Languages", ItemConverterType = typeof(StringEnumConverter))]
        public LANGUAGES[] Languages { get; set; }

        




        /// <summary>
        /// Gets or sets the ocr zones
        /// </summary>
        public OcrZone[] OcrZones { get; set; } = new OcrZone[] { };

        public override string ToString()
        {
            return this.ToJson();
        }
    }
}
