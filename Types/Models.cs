﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nuance.Omnipage.SampleCode.Types;

namespace Com.BancoAzteca.OCR.Types
{
    public class Models
    {
        public class TemplateDataForBd {
            public string IsMatched { get; set; }
            public string Confidence { get; set; }
            public string Name { get; set; }
            public string MatchedFileName { get; set; }
            public List<ZoneDataForBd> Zones { get; set; }

            public TemplateDataForBd(string IdType) {
                IsMatched = string.Empty;

                switch (IdType) {
                    case Constants.DocumentNames.CredencialIfe:
                        Zones = new List<ZoneDataForBd>() {
                                new ZoneDataForBd(){ Name = "Nombre"},
                                new ZoneDataForBd(){ Name = "Domicilio"},
                                new ZoneDataForBd(){ Name = "ClaveElector"},
                                new ZoneDataForBd(){ Name = "Curp"},
                                new ZoneDataForBd(){ Name = "EstadoId"},
                                new ZoneDataForBd(){ Name = "MunicipioId"},
                                new ZoneDataForBd(){ Name = "Sexo"},
                                new ZoneDataForBd(){ Name = "AnoRegistro"},
                                new ZoneDataForBd(){ Name = "NumeroEmision"},
                                new ZoneDataForBd(){ Name = "Seccion"},
                                new ZoneDataForBd(){ Name = "Localidad"},
                                new ZoneDataForBd(){ Name = "AnoEmision"},
                                new ZoneDataForBd(){ Name = "Vigencia"},
                                new ZoneDataForBd(){ Name = "FolioFrente"},
                                new ZoneDataForBd(){ Name = "Folio"},
                                new ZoneDataForBd(){ Name = "MRZ"},
                                new ZoneDataForBd(){ Name = "FullPageOcr"}
                            };
                        break;
                    case Constants.DocumentNames.CedulaProfesional:
                        Zones = new List<ZoneDataForBd>() {
                                new ZoneDataForBd(){ Name = "Nombre"},
                                new ZoneDataForBd(){ Name = "ApellidoPaterno"},
                                new ZoneDataForBd(){ Name = "ApellidoMaterno"},
                                new ZoneDataForBd(){ Name = "Curp"},
                                new ZoneDataForBd(){ Name = "Folio"}
                            };
                        break;
                    case Constants.DocumentNames.Pasaporte:
                        Zones = new List<ZoneDataForBd>() {
                                new ZoneDataForBd(){ Name = "FullPageOcr"}
                            };
                        break;
                    case Constants.DocumentNames.ComprobanteDomicilio:
                        Zones = new List<ZoneDataForBd>() {
                                new ZoneDataForBd(){ Name = "Nombre"},
                                new ZoneDataForBd(){ Name = "Calle"},
                                new ZoneDataForBd(){ Name = "EntreCalle"},
                                new ZoneDataForBd(){ Name = "Colonia"},
                                new ZoneDataForBd(){ Name = "CP"},
                                new ZoneDataForBd(){ Name = "Delegacion"}
                            };
                        break;
                    case "FullPageOcr":
                        Zones = new List<ZoneDataForBd>() {
                                new ZoneDataForBd(){ Name = "FullPageOcr"}
                            };
                        break;
                }
            }
        }

        public class ZoneDataForBd {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Confidence { get; set; }

            public ZoneDataForBd() {
                Name = null;
                Value = null;
                Confidence = null;
            }
        }
    }
}
