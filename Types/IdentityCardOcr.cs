﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System;
    using System.Linq;

    public class IdentityCardOcr : IdentityDocumentOcr
    {
        public override void DisplayData(IdentityDocumentBase passport)
        {
            IdentityCard identityCard = passport as IdentityCard;

            if (identityCard == null)
            {
                throw new ArgumentException($"{nameof(passport)} must be of type IdentityCard");
            }

            Display(identityCard);
        }

        public static void Display(IdentityCard identityCard)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("MRZ lines:");
            Console.WriteLine($"{identityCard.MrzLines[0]}");
            Console.WriteLine($"{identityCard.MrzLines[1]}");
            Console.WriteLine($"{identityCard.MrzLines[2]}\n");

            Console.WriteLine($"        Name: {identityCard.Surname}, {string.Join(" ", identityCard.GivenNames ?? new string[] { })}");
            Console.WriteLine($"      Gender: {identityCard.Sex}");
            Console.WriteLine($"     Country: {identityCard.CountryCode}");
            Console.WriteLine($"         DOB: {identityCard.Dob:yyyy-MM-dd}");
            Console.WriteLine($" Document id: {identityCard.DocumentNumber}");
            Console.WriteLine($"       Valid: {identityCard.ExpirationDate:yyyy-MM-dd}");
            Console.WriteLine($" Nationality: {identityCard.Nationality}");
            Console.WriteLine($"  Optional 1: {identityCard.FirstLineOptional}");
            Console.WriteLine($"  Optional 2: {identityCard.SecondLineOptional}");

            Console.WriteLine("\nCheck digits (ocr value - calculated)");
            Console.WriteLine($" Document id: {identityCard.DocumentNumberCheckDigit} - {identityCard.DocumentNumberCheckDigitCalculated}");
            Console.WriteLine($"         DOB: {identityCard.DobCheckDigit} - {identityCard.DobCheckDigitCalculated}");
            Console.WriteLine($" Expiry date: {identityCard.ExpiryDateCheckDigit} - {identityCard.ExpiryDateCheckDigitCalculated}");
            Console.WriteLine($"    Checksum: {identityCard.CheckDigit} - {identityCard.CheckDigitCalculated}");
            Console.WriteLine($"    Accuracy: {identityCard.CheckWeight * 100.0 / 4}%");

            Console.ForegroundColor = ConsoleColor.White;
        }

        public override string GetMrzLines(string imagePath)
        {         
            var recognizedText = GetRecognizedText(imagePath, true);

            var micrLines = recognizedText
                .Select(text => text.Split('\n').Select(line => line.Trim().Replace(" ", string.Empty)).Where(line => line.Length == 30).Reverse().Take(3).Reverse().ToArray())
                .Where(lines => lines.Length == 3)                
                .ToArray();

            return micrLines.Select(x => string.Join("\n", x)).FirstOrDefault();
        }
    }
}
