﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System;
    using System.Linq;
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    public class IdentityDocumentBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mrzLines"></param>
        /// <param name="mrzLineCount"></param>
        /// <param name="debugToConsole">
        /// If it's needed in the output
        /// </param>
        public IdentityDocumentBase(string mrzLines, int mrzLineCount, bool debugToConsole = true)
        {
            MrzLines = mrzLines.Replace(" ", string.Empty).Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

            if (MrzLines.Length != mrzLineCount)
            {
                MrzLines = mrzLines.Replace(" ", string.Empty).Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Take(3).ToArray();
            }

            var invalidCharacters = string.Join(string.Empty, MrzLines).Except(Constants.DigitValues.Keys).ToArray();

            InvalidCharactersFound = invalidCharacters.Any();

            if (InvalidCharactersFound && debugToConsole)
            {
                Console.WriteLine($"Invalid chars: {String.Join(",", invalidCharacters)}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool InvalidCharactersFound { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IdentityDocumentBase()
        {            
        }

        /// <summary>
        /// Gets the country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets the Date of Birth.
        /// </summary>
        public DateTime Dob { get; set; }

        /// <summary>
        /// Gets the passport number.
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public string DocumentNumberCheckDigit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public string DocumentNumberCheckDigitCalculated { get; set; }

        /// <summary>
        /// Gets the surname.
        /// </summary>
        public string Surname { get; set; }
        
        /// <summary>
        /// Gets the surname.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// The sex.
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Gets the given names.
        /// </summary>        
        public string[] GivenNames { get; set; }

        /// <summary>
        /// Gets the expiration date.
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] MrzLines { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CheckDigit[] CheckDigits { get; set; }

        /// <summary>Calculates the check digit.</summary>
        /// <param name="text">The text.</param>
        /// <returns>The check digit calculated from the text parameter.</returns>
        protected char GetCheckDigit(string text)
        {

            //try
            //{
            //    return Enumerable.Range(0, text.Length).Sum(x => Constants.DigitValues[text[x]] * Constants.Weights[x % 3]).ToString().Last();
            //}
            //catch(Exception e)
            //{
            //    return new char
            //}
            
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }
            else
            {
                
                return Enumerable.Range(0, text.Length).Sum(x => Constants.DigitValues[text[x]] * Constants.Weights[x % 3]).ToString().Last();
            }
        }

        protected bool CheckDigitForPassport(string input)
        {
            int[] weights = { 7, 3, 1 };
            char[] characters = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            int totalValue = 0;

            for (int index = 0; index < input.Length - 1; index++)
                if (input[index] != '<')
                    totalValue += Array.FindIndex<char>(characters, delegate (char c) { return input[index] == c; }) * weights[index % 3];

            return totalValue % 10 == int.Parse(input[input.Length - 1].ToString());
        }

        /// <summary>The fix digits.</summary>
        /// <param name="text">The text.</param>
        /// <param name="replaceMap">The replace Map.</param>        
        protected string FixDigits(string text, string replaceMap = "O0:I1:l1")
        {
            return replaceMap.Split(':').Aggregate(text, (current, map) => current.Replace(map[0], map[1]));
        }

        public static string GetCountryByCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return "Unknown";

            string countryName = string.Empty;

            Constants.Countries.TryGetValue(code, out countryName);

            if (!string.IsNullOrEmpty(countryName))
                return countryName;
            else
                return code;
        }
    }
}