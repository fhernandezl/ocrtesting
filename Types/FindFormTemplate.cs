﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System.Xml.Serialization;
    using System.Collections.Generic;
    namespace TemplateEngine
    {
        [XmlRoot(ElementName = "wd")]
        public class Wd
        {
            [XmlAttribute(AttributeName = "l")]
            public string L { get; set; }
            [XmlAttribute(AttributeName = "t")]
            public string T { get; set; }
            [XmlAttribute(AttributeName = "r")]
            public string R { get; set; }
            [XmlAttribute(AttributeName = "b")]
            public string B { get; set; }
            [XmlAttribute(AttributeName = "OCR")]
            public string Ocr { get; set; }
        }

        [XmlRoot(ElementName = "RecognizedWords")]
        public class RecognizedWords
        {
            [XmlElement(ElementName = "wd")]
            public List<Wd> Wd { get; set; }
        }

        [XmlRoot(ElementName = "anchor")]
        public class Anchor
        {
            [XmlElement(ElementName = "RecognizedWords")]
            public RecognizedWords RecognizedWords { get; set; }
            [XmlAttribute(AttributeName = "zoneindex")]
            public string Zoneindex { get; set; }
            [XmlAttribute(AttributeName = "zonename")]
            public string Zonename { get; set; }
            [XmlAttribute(AttributeName = "type")]
            public string Type { get; set; }
            [XmlAttribute(AttributeName = "content")]
            public string Content { get; set; }
            [XmlAttribute(AttributeName = "driftX")]
            public string DriftX { get; set; }
            [XmlAttribute(AttributeName = "driftY")]
            public string DriftY { get; set; }
            [XmlAttribute(AttributeName = "value")]
            public string Value { get; set; }
            [XmlAttribute(AttributeName = "err")]
            public string Err { get; set; }
            [XmlAttribute(AttributeName = "l")]
            public string L { get; set; }
            [XmlAttribute(AttributeName = "t")]
            public string T { get; set; }
            [XmlAttribute(AttributeName = "r")]
            public string R { get; set; }
            [XmlAttribute(AttributeName = "b")]
            public string B { get; set; }
            [XmlAttribute(AttributeName = "dl")]
            public string Dl { get; set; }
            [XmlAttribute(AttributeName = "dt")]
            public string Dt { get; set; }
            [XmlAttribute(AttributeName = "dr")]
            public string Dr { get; set; }
            [XmlAttribute(AttributeName = "db")]
            public string Db { get; set; }
            [XmlAttribute(AttributeName = "OCR")]
            public string Ocr { get; set; }
            [XmlAttribute(AttributeName = "regExp")]
            public string RegExp { get; set; }
        }

        [XmlRoot(ElementName = "nonmatch")]
        public class Nonmatch
        {
            [XmlElement(ElementName = "anchor")]
            public List<Anchor> Anchor { get; set; }
            [XmlAttribute(AttributeName = "confidence")]
            public string Confidence { get; set; }
            [XmlAttribute(AttributeName = "err")]
            public string Err { get; set; }
        }

        [XmlRoot(ElementName = "Template")]
        public class Template
        {
            [XmlElement(ElementName = "nonmatch")]
            public Nonmatch Nonmatch { get; set; }
            [XmlAttribute(AttributeName = "name")]
            public string Name { get; set; }
            [XmlAttribute(AttributeName = "index")]
            public string Index { get; set; }
            [XmlAttribute(AttributeName = "enabled")]
            public string Enabled { get; set; }
            [XmlAttribute(AttributeName = "matchings")]
            public int Matchings { get; set; }
            [XmlAttribute(AttributeName = "select")]
            public string Select { get; set; }
            [XmlElement(ElementName = "match")]
            public Match Match { get; set; }
        }

        [XmlRoot(ElementName = "match")]
        public class Match
        {
            [XmlElement(ElementName = "anchor")]
            public List<Anchor> Anchor { get; set; }
            [XmlAttribute(AttributeName = "ID")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "confidence")]
            public string Confidence { get; set; }
            [XmlAttribute(AttributeName = "err")]
            public string Err { get; set; }
            [XmlAttribute(AttributeName = "rank")]
            public string Rank { get; set; }
        }

        [XmlRoot(ElementName = "Templates")]
        public class Templates
        {
            [XmlElement(ElementName = "Template")]
            public List<Template> Template { get; set; }
        }

        [XmlRoot(ElementName = "FindFormTemplate")]
        public class FindFormTemplate
        {
            [XmlElement(ElementName = "Templates")]
            public Templates Templates { get; set; }
        }

    }

}
