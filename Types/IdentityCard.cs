﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// The passport.
    /// </summary>
    public class IdentityCard : IdentityDocumentBase
    {
        public IdentityCard()
        {

        }

        public static bool IsMrz(string text)
        {
            return (text ?? string.Empty).Replace(" ", string.Empty).Split('\n').Select(line => line.Trim()).Count(line => line.Length >= 30 && line.Length <=38) == 3;
        }

        //public static bool IsMrzForPassport(string text)
        //{
        //    return (text ?? string.Empty).Replace(" ", string.Empty).Split('\n').Select(line => line.Trim()).Count(line => line.Length >= 42 && line.Length <= 44) == 2;
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="Passport"/> class.
        /// </summary>
        /// <param name="mrzLines">
        /// The mrp lines.
        /// </param>
        /// <param name="debugToConsole">
        /// if true then writes debug info to the Console.
        /// </param>
        public IdentityCard(string mrzLines, bool debugToConsole = false) : base(mrzLines, 3)
        {
            try
            {
                // 1st line
                MrzStart = MrzLines[0].Substring(0, 1);
                IdType = MrzLines[0].Substring(1, 1);
                CountryCode = MrzLines[0].Substring(2, 3);
                DocumentNumber = MrzLines[0].Substring(5, 9).Replace("<", String.Empty);
                DocumentNumberCheckDigit = MrzLines[0].Substring(14, 1);
                DocumentNumberCheckDigitCalculated = new String(GetCheckDigit(MrzLines[0].Substring(5, 9)), 1);
                FirstLineOptional = MrzLines[0].Substring(15, 15).Replace("<", String.Empty);
                ExpiryDateCheckDigit = MrzLines[1][14];
                ExpiryDateCheckDigitCalculated = GetCheckDigit(MrzLines[1].Substring(8, 6));

                // 2nd line
                Dob = DateTime.ParseExact(FixDigits(MrzLines[1].Substring(0, 6)), "yyMMdd", CultureInfo.InvariantCulture);
                DobCheckDigit = MrzLines[1][6];
                DobCheckDigitCalculated = GetCheckDigit(MrzLines[1].Substring(0, 6));
                Sex = MrzLines[1].Substring(7, 1);
                ExpiryDate = FixDigits(MrzLines[1].Substring(8, 6));
                ExpirationDate = DateTime.ParseExact(ExpiryDate, "yyMMdd", CultureInfo.InvariantCulture);
                Nationality = MrzLines[1].Substring(15, 3);
                SecondLineOptional = MrzLines[1].Substring(18, 11).Replace("<", String.Empty);

                // 3rd line                
                var nameIndex = MrzLines[2].IndexOf("<<", StringComparison.Ordinal);

                Surname = MrzLines[2].Substring(0, nameIndex).Replace('<', ' ');
                GivenNames = MrzLines[2].Substring(nameIndex + 2).Split(new[] { '<' }, StringSplitOptions.RemoveEmptyEntries);

                CheckDigit = MrzLines[1][29];
                CheckDigitCalculated = InvalidCharactersFound
                ? '_'
                : GetCheckDigit($"{MrzLines[0].Substring(5, 25)}{FixDigits(MrzLines[1].Substring(0, 7))}{FixDigits(MrzLines[1].Substring(8, 7))}{FixDigits(MrzLines[1].Substring(18, 11))}");

                //if (documentType == DOCUMENT_TYPE.MexicanVoterCard)
                //{


                //}
                //else if (documentType == DOCUMENT_TYPE.Passport)
                //{
                //    // 1st line
                //    MrzStart = MrzLines[0].Substring(0, 1);
                //    IdType = MrzLines[0].Substring(0, 1);
                //    CountryCode = MrzLines[0].Substring(2, 3);
                //    Surname = MrzLines[0].Length > 5 && Regex.IsMatch(MrzLines[0].Substring(5), "^[A-Z?]+(<[A-Z?]+)*<<[A-Z?]") ? Regex.Match(MrzLines[0].Substring(5), "^[A-Z?]+(<[A-Z?]+)*").Value.Replace('<', ' ') : "?";
                //    Firstname = MrzLines[0].Length > 5 && Regex.IsMatch(MrzLines[0].Substring(5) + "<", "[A-Z?]<<[A-Z?]+(<[A-Z?]+)*(<+$|<<)") ? Regex.Match(MrzLines[0].Substring(5), "<<[A-Z?]+(<[A-Z?]+)*").Value.Substring(2).Replace('<', ' ') : "?";

                //    // 2nd line
                //    DocumentNumber = Regex.IsMatch(MrzLines[1], "^[A-Z0-9<]{9}[0-9]") && CheckDigitForPassport(MrzLines[1].Substring(0, 10)) ? MrzLines[1].Substring(0, 9).Replace("<", string.Empty) : "?";
                //    Dob = DateTime.ParseExact(FixDigits(MrzLines[1].Substring(12, 6)), "yyMMdd", CultureInfo.InvariantCulture);
                //    DobCheckDigit = MrzLines[1][19];
                //    DobCheckDigitCalculated = GetCheckDigit(MrzLines[1].Substring(12, 6));
                //    Sex = MrzLines[1].Substring(20, 1);
                //    ExpiryDate = FixDigits(MrzLines[1].Substring(21, 6));
                //    ExpirationDate = DateTime.ParseExact(ExpiryDate, "yyMMdd", CultureInfo.InvariantCulture);
                //    Nationality = MrzLines[1].Substring(10, 3);
                //    ExpiryDateCheckDigit = MrzLines[1][27];
                //    ExpiryDateCheckDigit = GetCheckDigit(MrzLines[1].Substring(21, 6));
                //}

                CheckDigits = new[]
                   {
                    new CheckDigit
                    {
                        Name = "DocumentNumber",
                        Value = DocumentNumberCheckDigit,
                        Calculated = DocumentNumberCheckDigitCalculated
                    },
                    new CheckDigit
                    {
                        Name = "DateOfBirth",
                        Value = new String(DobCheckDigit, 1),
                        Calculated = new String(DobCheckDigitCalculated, 1)
                    },
                    new CheckDigit
                    {
                        Name = "ExpirationDate",
                        Value = new String(ExpiryDateCheckDigit, 1),
                        Calculated = new String(ExpiryDateCheckDigitCalculated, 1)
                    },
                    new CheckDigit
                    {
                        Name = "CheckDigit",
                        Value = new String(CheckDigit, 1),
                        Calculated = new String(CheckDigitCalculated, 1)
                    }
                };
            }
            catch (Exception ex)
            {
                //Com.BancoAzteca.OCR/clsLog.ArchiveLog("Falló en inicializar el objeto Identificacion debido a: " + ex.Message);
                Com.BancoAzteca.OCR.clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", "Falló en inicializar el objeto Identificacion debido a: " + ex.Message);
                if (debugToConsole)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public string SecondLineOptional { get; set; }

        public string Nationality { get; set; }

        public string FirstLineOptional { get; set; }

        public string IdType { get; set; }

        public string MrzStart { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char DobCheckDigit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char DobCheckDigitCalculated { get; set; }

        /// <summary>
        /// Gets or sets the expiry date. 9-1, 6, Expiration date of document (YYMMDD)
        /// </summary>        
        private string ExpiryDate { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public char ExpiryDateCheckDigit { get; set; } //=> MrzLines[1][14];

        [XmlIgnore]
        [JsonIgnore]
        public char ExpiryDateCheckDigitCalculated { get; set; } //=> GetCheckDigit(MrzLines[1].Substring(8, 6));

        /// <summary>
        /// Check digit over digits 1–10, 14–20, and 22–43
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char CheckDigit { get; set; }// => MrzLines[1][29];

        /// <summary>
        /// The check digit calculated.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public char CheckDigitCalculated { get; set; }/*=>
            InvalidCharactersFound
                ? '_'
                : GetCheckDigit($"{MrzLines[0].Substring(5, 25)}{FixDigits(MrzLines[1].Substring(0, 7))}{FixDigits(MrzLines[1].Substring(8, 7))}{FixDigits(MrzLines[1].Substring(18, 11))}");
                */
        [JsonIgnore]
        public int CheckWeight =>
            (DocumentNumberCheckDigit == DocumentNumberCheckDigitCalculated ? 1 : 0)
            + (DobCheckDigit == DobCheckDigitCalculated ? 1 : 0)
            + (ExpiryDateCheckDigit == ExpiryDateCheckDigitCalculated ? 1 : 0)
            + (CheckDigit == CheckDigitCalculated ? 1 : 0);

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The class ToString
        /// </returns>
        public override string ToString()
        {
            return $"{Surname}, {string.Join(" ", GivenNames)} ({Sex})\nCountry: {CountryCode}\nDate of birth: {Dob:yyyy-MM-dd}\nPassport number: {DocumentNumber}\nExpiry date: {ExpirationDate:yyyy-MM-dd}";
        }
    }
}
