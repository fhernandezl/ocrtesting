﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class XmlCommentAttribute : Attribute
    {
        public XmlCommentAttribute(string value)
        {
            Value = value;
        }

        public string Value { get; set; }
    }
}