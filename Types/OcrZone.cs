﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using OmniPage.CSDK.ArgTypes;
    using System.Xml.Serialization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System;
    using OmniPage.CSDK.Objects;

    public class OcrZone
    {
        public OcrZone()
        {
        }

        public OcrZone(OCRZone zone)
        {
            Index = zone.Index;
            Name = zone.Name ?? zone.FormFieldName;
            if (zone.Name == "Domicilio")
                Text = zone.GetZoneText().TrimEnd();
            else
                Text = zone.GetZoneText(false).TrimEnd();
            Filter = zone.GetZoneInfo().filter;
            FillingMethod = zone.GetZoneInfo().fm;
            BoundingRect = zone.GetZoneInfo().rectBBox;
            RecognitionModule = zone.GetZoneInfo().rm;
            ZoneType = zone.GetZoneInfo().type;
            FilterPlus = null;
            CustomValue = null;
            Zone = zone;

            if (zone.Attributes.ContainsKey("Regex"))
            {
                RegexPattern = zone.Attributes["Regex"];
            }

            RefreshZone();
    
            ZoneConfidence = zone.GetZoneConfidence();
        }

        public void RefreshZone()
        {
            // try to find the best alternative word if regex which matches the regex.
            if (!string.IsNullOrEmpty(RegexPattern))
            {
                var word = Zone.GetWords().FirstOrDefault(w => w.Text.Equals(Text));

                if (word != null)
                {
                    var bestAlternative = word.Alternatives.FirstOrDefault(w => Regex.IsMatch(w, RegexPattern));
                    if (!String.IsNullOrEmpty(bestAlternative) && !bestAlternative.Equals(Text))
                    {
                        //Console.WriteLine($"{Text} changed to {bestAlternative}");
                        Text = bestAlternative;
                    }
                }
            }

            if (!string.IsNullOrEmpty(Name)) {
                if (Name.Equals("CURP", StringComparison.InvariantCultureIgnoreCase))
                {
                    Text = FixCurp(Text);
                }
            }
            if (!string.IsNullOrEmpty(RegexPattern))
            {
                IsMatch = Regex.IsMatch(Text ?? string.Empty, RegexPattern);
            }
        }


        private static string FixCurp(string curp)
        {
            //\\b[A-Z]{4}[0-9]{6}[HM]{1}[A-Z]{5}[0-9]{2}\\b
            var cleanCurp = Regex.Replace((curp), @"[^A-Z0-9]+", "");
            //CODE MADE IN ORDER TO FIX LONG LENGTH CURP, ALSO NEED CHECKING WHY IT'S GIVING CLAVE DE ELECTOR INFO @fhernandezl
            if (cleanCurp.Length == 18)
            {
                return $"{cleanCurp.Substring(0, 4).FixLetters()}" +
                       $"{cleanCurp.Substring(4, 6).FixDigits()}" +
                       $"{cleanCurp.Substring(10, 1)}" +
                       $"{cleanCurp.Substring(11, 5).FixLetters()}" +
                       $"{cleanCurp.Substring(16, 2).FixDigits()}";
            }

            return cleanCurp;
        }

        /// <summary>
        /// Gets or sets the zone index
        /// </summary>
        [XmlAttribute]
        public int Index { get; set; }

        /// <summary>
        /// Get or sets the zone name
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the zone text
        /// </summary>
        public string Text { get; set; }

        public double ZoneConfidence { get; set; }

        /// <summary>
        /// Gets or sets the zone filter
        /// </summary>
        [XmlAttribute]
        [JsonConverter(typeof(StringEnumConverter))]
        public CHR_FILTER Filter { get; set; }

        /// <summary>
        /// Gets or sets the fillingmethod
        /// </summary>
        [XmlAttribute]
        [JsonConverter(typeof(StringEnumConverter))]
        public FILLINGMETHOD FillingMethod { get; set; }

        /// <summary>
        /// Gets or sets the bounding rectangle.
        /// </summary>        
        public RECT BoundingRect { get; set; }

        /// <summary>
        /// Gets or sets the recognition module.
        /// </summary>
        [XmlAttribute]
        [JsonConverter(typeof(StringEnumConverter))]
        public RECOGNITIONMODULE RecognitionModule { get; set; }

        /// <summary>
        /// Gets or sets the zone type.
        /// </summary>
        [XmlAttribute]
        [JsonConverter(typeof(StringEnumConverter))]
        public ZONETYPE ZoneType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FilterPlus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "CustomValue", Type = typeof(IdentityCard))]
        public object CustomValue { get; set; }

        public OCRZone Zone { get; }

        public string RegexPattern { get; set; }

        public bool? IsMatch { get; set; }
    }
}
