﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Text;
using Nuance.Omnipage.SampleCode.Types;

namespace Com.BancoAzteca.OCR.Types
{
    public class XmlResponse
    {
        public class XmlPage {
            public List<KeyValuePair<string, string>> Page { get; set; }
            public string Template { get; set; }
            public string Side { get; set; }

            public XmlPage() {
                Page = null;
                Template = null;
                Side = null;
            }
        }

        public static string GenerateResponseXml(List<XmlPage> xmlPages) {
            XmlDocument DocumentXml = new XmlDocument();
            DocumentXml.AppendChild(DocumentXml.CreateXmlDeclaration("1.0", "UTF-8", null));
            DocumentXml.AppendChild(DocumentXml.CreateElement("Countries"));

            if (xmlPages.Count > 1) {
                if (xmlPages[0].Template.Split('_')[0] != xmlPages[1].Template.Split('_')[0]) return "4";
            }

            XmlNode countryNode = DocumentXml.CreateElement("Country");
            countryNode.Attributes.Append(DocumentXml.CreateAttribute("Name"));
            countryNode.Attributes["Name"].Value = IdentityDocumentBase.GetCountryByCode(xmlPages[0].Template.Split('_')[1]);
            DocumentXml.DocumentElement.AppendChild(countryNode);

            XmlNode documentNode = DocumentXml.CreateElement("Document");
            documentNode.Attributes.Append(DocumentXml.CreateAttribute("Name"));
            documentNode.Attributes["Name"].Value = xmlPages[0].Template.Split('_')[0];
            countryNode.AppendChild(documentNode);

            XmlNode pageNode = DocumentXml.CreateElement("Page");
            pageNode.Attributes.Append(DocumentXml.CreateAttribute("Number"));
            pageNode.Attributes["Number"].Value = "1";
            documentNode.AppendChild(pageNode);

            foreach (var zone in xmlPages[0].Page)
            {
                XmlNode newNode = DocumentXml.CreateElement(zone.Key);
                newNode.InnerText = zone.Value;

                pageNode.AppendChild(newNode);
            }

            if (xmlPages.Count > 1) {
                XmlNode page2Node = DocumentXml.CreateElement("Page");
                page2Node.Attributes.Append(DocumentXml.CreateAttribute("Number"));
                page2Node.Attributes["Number"].Value = "2";
                documentNode.InsertAfter(page2Node, pageNode);

                foreach (var zone in xmlPages[1].Page)
                {
                    XmlNode newNode = DocumentXml.CreateElement(zone.Key);
                    newNode.InnerText = zone.Value;

                    page2Node.AppendChild(newNode);
                }
            }

            return DocumentXml.InnerXml;
        }
    }
}
