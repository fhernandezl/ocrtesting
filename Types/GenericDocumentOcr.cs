﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CrRNRBAZDIGOCRPostProcessing.Models;
using Nuance.Omnipage.SampleCode;
using Nuance.Omnipage.SampleCode.Types;
using Nuance.OmniPage.CSDK.Objects;

namespace Com.BancoAzteca.OCR.Types
{
    public class GenericDocumentOcr
    {
        [XmlIgnore]
        public Nuance.Omnipage.SampleCode.Types.OcrPage[] OcrResults { get; private set; }
        public string[] Images { get; }
        [XmlIgnore]
        public string[] Templates { get; }
        [XmlIgnore]
        public SettingCollection[] SettingProfiles { get; set; }

        public List<KeyValuePair<string, string>> FrontPageZonesAndValues { get; set; }
        public List<KeyValuePair<string, string>> BackPageZonesAndValues { get; set; }

        public bool FinishedFrontPageOperations = false;
        public bool FinishedBackPageOperations = false;

        public string FrontTemplateName { get; set; }
        public string BackTemplateName { get; set; }

        private IEnumerable<OcrPackage> GetOcrPackages()
        {
            return from setting in SettingProfiles
                   from file in Images
                   from template in Templates
                   select new OcrPackage
                   {
                       FileName = file,
                       Settings = setting,
                       Template = template
                   };
        }

        public GenericDocumentOcr(string[] images, string[] templates)
        {
            Images = images;
            Templates = templates;
        }

        public void Process()
        {
            OcrResults = GetOcrPackages().AsParallel().WithDegreeOfParallelism(Program.Paralellismlevel)
                .Select(ocr => TemplatesWithFilterPlus.MatchTemplates(ocr.Template, ocr.FileName, ocr.Settings))
                .ToArray();
            OCRProcessor.matchingtemplate = OcrResults.Any(p => p.FormTemplateName != "No matching template");
        }

        /// <summary>
        ///OCR Extraction is done here
        /// </summary>
        public void ExtractData()
        {
            var templateName = OcrResults.Where(t=> t.FormTemplateName != "No matching template").OrderByDescending(t => t.Confidence).FirstOrDefault()?.FormTemplateName;

            var bestMatchingResults = OcrResults.Where(t => t.FormTemplateName != "No matching template").OrderByDescending(t => t.Confidence).FirstOrDefault();

            List<KeyValuePair<string, string>> ZonesAndValues = new List<KeyValuePair<string, string>>();

            foreach (var zona in bestMatchingResults.OcrZones)
            {
                ZonesAndValues.Add(new KeyValuePair<string, string>(zona.Name, zona.Text));
            }

            string documentside = templateName.Contains("Front") ? "Front" : "Back";

            if (templateName.Contains("Front"))
            {
                FrontPageZonesAndValues = ZonesAndValues;
                FinishedFrontPageOperations = true;
                FrontTemplateName = templateName;
            }
            else
            {
                BackPageZonesAndValues = ZonesAndValues;
                FinishedBackPageOperations = true;
                BackTemplateName = templateName;
            }
        }

        public bool IsTemplateValid()
        {
            return (OcrResults.Where(t => t.FormTemplateName != "No matching template").OrderByDescending(t => t.Confidence).FirstOrDefault()?.FormTemplateName != "No matching template") ;
        }

        internal bool IsImageDocumentTheSameAsIdDocument()
        {
            if (OcrResults.Where(t => t.FormTemplateName != "No matching template").OrderByDescending(t => t.Confidence).FirstOrDefault()?.FormTemplateName != "No matching template")
            {
                string documentKey = OcrResults.Where(t => t.FormTemplateName != "No matching template").OrderByDescending(t => t.Confidence).FirstOrDefault()?.FormTemplateName?.Split('_')[0];
                string nombreDocumento;

                Constants.GenericDocumentsAbrevs.TryGetValue(documentKey, out nombreDocumento);

                return !string.IsNullOrEmpty(nombreDocumento);
            }

            return false;
        }

        internal string GetFinalXml(List<KeyValuePair<string,string>> frontPage, string frontTemplateName, List<KeyValuePair<string, string>> BackPage, string backTemplateName)
        {
            List<XmlResponse.XmlPage> xmlPages = new List<XmlResponse.XmlPage>();

            if (frontPage != null)
                xmlPages.Add(new XmlResponse.XmlPage() { Page = frontPage, Side = "Front", Template = frontTemplateName });

            if (BackPage != null)
                xmlPages.Add(new XmlResponse.XmlPage() { Page = BackPage, Side = "Back", Template = backTemplateName });

            return XmlResponse.GenerateResponseXml(xmlPages);
        }
    }
}
