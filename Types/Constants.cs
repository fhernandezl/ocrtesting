﻿namespace Nuance.Omnipage.SampleCode.Types
{
    using System.Collections.Generic;

    public static class Constants
    {
        public static readonly Dictionary<char, byte> DigitValues = new Dictionary<char, byte>()
            {
                { '0', 0 },
                { '1', 1 },
                { '2', 2 },
                { '3', 3 },
                { '4', 4 },
                { '5', 5 },
                { '6', 6 },
                { '7', 7 },
                { '8', 8 },
                { '9', 9 },
                { 'A', 10 },
                { 'B', 11 },
                { 'C', 12 },
                { 'D', 13 },
                { 'E', 14 },
                { 'F', 15 },
                { 'G', 16 },
                { 'H', 17 },
                { 'I', 18 },
                { 'J', 19 },
                { 'K', 20 },
                { 'L', 21 },
                { 'M', 22 },
                { 'N', 23 },
                { 'O', 24 },
                { 'P', 25 },
                { 'Q', 26 },
                { 'R', 27 },
                { 'S', 28 },
                { 'T', 29 },
                { 'U', 30 },
                { 'V', 31 },
                { 'W', 32 },
                { 'X', 33 },
                { 'Y', 35 },
                { 'Z', 35 },
                { '<', 0 },
                {'?',  36 }
            };

        public static readonly int[] Weights = { 7, 3, 1 };

        public class DocumentNames {
            public const string Pasaporte = "Pasaporte";
            public const string CredencialIfe = "Credencial IFE";
            public const string ComprobanteDomicilio = "Comprobante de Domicilio del Cliente";
            public const string CedulaProfesional = "Cedula Profesional";
        }

        public static Dictionary<string, string> Countries = new Dictionary<string, string>() {
            { "MEX","MEXICO"},
            { "ISR","ISRAEL"},
            { "USA","ESTADOS UNIDOS"},
            { "ARG","ARGENTINA"},
            { "COL","COLOMBIA"},
            { "FRA","FRANCIA"},
            { "CHL","CHILE"},
            { "ESP","ESPAÑA"},
            { "ITA","ITALIA"},
            { "BLZ","BELICE"},
            { "HND","HONDURAS"},
            { "HTI","HAITI"},
            { "CAN","CANADA"},
            { "BRA","BRASIL"},
            { "AUS","AUSTRALIA"},
            { "SLV","EL SALVADOR"},
            { "URY","URUGUAY"},
            { "GTM","GUATEMALA"},
            { "ROU","RUMANIA"},
            { "CHN","CHINA"},
            { "CRI","COSTA RICA"},
            { "HUN","HUNGRIA"},
            { "PER","PERU"},
            { "NIC","NICARAGUA"},
            { "DOM","REPUBLICA DOMINICANA"},
            { "CUB","CUBA"},
            { "JAM","JAMAICA"},
            { "PAN","PANAMA"},
            { "ECU","ECUADOR"},
            { "VEN", "VENEZUELA"} };

        public static Dictionary<string, string> GenericDocumentsAbrevs = new Dictionary<string, string>() {
            { "ComprobanteDomicilioCFE","Comprobante de Domicilio del Cliente"},
            { "ComprobanteDomicilioTelmex", "Comprobante de Domicilio del Cliente"},
            { "CedulaProfesional", "Cedula Profesional"}
        };
    }
}
