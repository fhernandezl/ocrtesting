﻿using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Serialization;
using CrRNRBAZDIGOCRPostProcessing;
using Com.BancoAzteca.OCR;
using CrRNRBAZDIGOCRPostProcessing.Models;
using System.Text.RegularExpressions;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class VotingCardOcr
    {
        private IdentityCard identityCard;

        public VotingCardOcr(string[] images, string[] templates)
        {
            Images = images;
            Templates = templates;
        }

        public void Process()
        {

            var stopwatch = Stopwatch.StartNew();


            OcrResults = GetOcrPackages().AsParallel().WithDegreeOfParallelism(Program.Paralellismlevel)
                .Select(ocr => TemplatesWithFilterPlus.MatchTemplates(ocr.Template, ocr.FileName, ocr.Settings))
                .ToArray();
           OCRProcessor.matchingtemplate = OcrResults.Any(p => p.FormTemplateName != "No matching template");

            var elapsedMilliseconds = stopwatch.ElapsedMilliseconds;
            Console.WriteLine($"{elapsedMilliseconds / 1000.0:0.00} seconds, {(elapsedMilliseconds / OcrResults.Count()):0.0 msec per page} ");
        }


        /// <summary>
        ///OCR Extraction is done here
        /// </summary>
        public void ExtractData()
        {
            //.WithDegreeOfParallelism(Program.Paralellismlevel)
            var firsPageResults = OcrResults.AsParallel().WithDegreeOfParallelism(Program.Paralellismlevel).Where(IsFrontPage).OrderByDescending(FirstPageWeight).ThenByDescending(p => p.Confidence).FirstOrDefault();

            if (firsPageResults != null)
            {
                    #region Log Resultados Puro
                    ExtensionMethods.GetRawExtractionData(OcrResults.Where(IsFrontPage).OrderByDescending(FirstPageWeight).ThenByDescending(p => p.Confidence).ToArray(), Constants.DocumentNames.CredencialIfe, firsPageResults.FormTemplateName);
                    #endregion
                
                SetFrontPageData(firsPageResults.OcrZones, false);
                FirstPage = firsPageResults;
                SetPostProcessingPage(firsPageResults, "front");
            }

            var lastPageResults = OcrResults.Where(IsBackPage).OrderByDescending(BackPageWeight).ThenByDescending(p => p.Confidence).FirstOrDefault();

            if (lastPageResults != null && firsPageResults == null)
            {
                
                    #region Log Resultados Puro

                    ExtensionMethods.GetRawExtractionData(
                        OcrResults.Where(IsBackPage).OrderByDescending(BackPageWeight)
                            .ThenByDescending(p => p.Confidence).ToArray(), Constants.DocumentNames.CredencialIfe,
                        lastPageResults.FormTemplateName);

                    #endregion
                
                if (lastPageResults.OcrZones.Any(z => z.Name.Equals("MRZ", StringComparison.InvariantCultureIgnoreCase)))
                {
                    var mrzZone = lastPageResults.OcrZones.First(z => z.Name.Equals("MRZ", StringComparison.InvariantCultureIgnoreCase));
                    if (IdentityCard.IsMrz(mrzZone.Text.Trim()))
                    {
                        identityCard = new IdentityCard(mrzZone.Text.Replace(" ", string.Empty));
                        var mrztext = $"{identityCard.Surname} {string.Join(" ", identityCard.GivenNames ?? new string[] { })}";
                        if (identityCard.MrzLines.Last().Last() == '<')
                        {
                            Nombre = $"{identityCard.Surname} {string.Join(" ", identityCard.GivenNames ?? new string[] { })}";
                        }
                        else
                        {
                            Nombre = Nombre?.Length > mrztext.Length ? Nombre : mrztext;
                        }

                         folioZone = new OcrZone { Text = identityCard.FirstLineOptional, IsMatch = true, Name = "Folio", ZoneConfidence = 100 };
                         SetPostProcessingPage(lastPageResults, "back");
                         Folio = identityCard.FirstLineOptional == null ? null : folioZone.Text;
                         mrzZone.CustomValue = identityCard;
                    }
                }
                else
                {
                    Folio = lastPageResults.OcrZones.FirstOrDefault(z => z.Name.Equals("Folio", StringComparison.InvariantCultureIgnoreCase))?.Text;

                    if (!string.IsNullOrEmpty(Folio)) {
                        
                        SetPostProcessingPage(lastPageResults, "back");
                        
                    }   
                }

                BackPage = lastPageResults;
            }
            else
            {
                if (firsPageResults == null)
                {
                    // Full page OCR for vertical folio and/or mrz           
                    var imageFile = Images.First();
                    //var forcedOcrZones = TemplatesWithFilterPlus.ForceApplyTemplate(imageFile, Templates.First(), "Type4_Back").ToArray();
                    //string ocrText = forcedOcrZones.Where(z => z.Name.Equals("Folio", StringComparison.OrdinalIgnoreCase)).Select(z => z.Text).FirstOrDefault();
                    var ocrText = TemplatesWithFilterPlus.LookForPattern(imageFile,@"\b\d{13}\b",new[] {
                                        new SettingCollection
                                            {
                                                //DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                                                RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                                                DefaultFillingMethod = FILLINGMETHOD.FM_OCRA,
                                                ImageResolutionEnhancement = IMG_RESENH.RE_AUTO
                                            },
                                        new SettingCollection
                                            {
                                                //DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                                                RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                                                DefaultFillingMethod = FILLINGMETHOD.FM_OCRA,
                                                ImageResolutionEnhancement = IMG_RESENH.RE_NO
                                            },
                                        new SettingCollection
                                            {
                                                //DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                                                RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                                                DefaultFillingMethod = FILLINGMETHOD.FM_OCRB,
                                                ImageResolutionEnhancement = IMG_RESENH.RE_NO
                                            },
                                        new SettingCollection
                                            {
                                                //DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                                                RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                                                DefaultFillingMethod = FILLINGMETHOD.FM_OCRB,
                                                ImageResolutionEnhancement = IMG_RESENH.RE_AUTO
                                            }
                                        });


                    Console.WriteLine(ocrText != null ? "Updating to" + ocrText : "Sin cambios");

                    Folio = ocrText;

                    if (!string.IsNullOrEmpty(ocrText))
                    {
                        OCRProcessor.matchingtemplate = true;
                        BackPage.FormTemplateName = "Full OCR Backpage IFE";
                        BackPage.OcrZones = new[] { new OcrZone { Name = "Folio", Text = Folio, IsMatch = true } };
                        folioZone = new OcrZone { Text = Folio, IsMatch = true, Name = "Folio", ZoneConfidence = 100 };
                        identityCard.DocumentNumber = "";
                        SetPostProcessingPage(lastPageResults, "back");

                        if (imageFile != null)
                        {
                            BackPage.FileName = imageFile;

                        }

                        OCRProcessor.matchingtemplate = true;
                    }


                }
            }

            if (string.IsNullOrEmpty(Folio) && firsPageResults == null)
            {

                var imageFile = Images.First();
                var ocrText = TemplatesWithFilterPlus.GetMrzFromPage(
                    imageFile,
                    new SettingCollection
                    {
                        DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                        RMTradeoff = RMTRADEOFF.TO_ACCURATE,
                        DefaultFillingMethod = FILLINGMETHOD.FM_OCRB,
                        ImageResolutionEnhancement = IMG_RESENH.RE_AUTO
                    }

                    );

                if (IdentityCard.IsMrz(ocrText))
                {
                    var identityCard = new IdentityCard(ocrText);
                    Folio = identityCard.FirstLineOptional;
                    BackPage.FormTemplateName = "Full OCR Backpage IFE";
                    BackPage.FileName = imageFile;
                    folioZone = new OcrZone { Text = identityCard.FirstLineOptional, IsMatch = true, Name = "Folio", ZoneConfidence = 100 };
                    
                    BackPage.OcrZones = new[] { folioZone };
                    Folio = folioZone.Text;
                    OCRProcessor.matchingtemplate = true;
                    SetPostProcessingPage(lastPageResults, "back",false,BackPage.FormTemplateName,BackPage.FileName,BackPage.OcrZones);

                }
                else
                {
                    var type4backpage = TemplatesWithFilterPlus.ForceApplyTemplate(imageFile, Templates.First(), "Type4_Back").ToArray();
                    ocrText = type4backpage.Where(z => z.Name.Equals("Folio", StringComparison.OrdinalIgnoreCase)).Select(z => z.Text).FirstOrDefault().FixDigits();

                    var type1backpage = TemplatesWithFilterPlus.ForceApplyTemplate(imageFile, Templates.First(), "Type1_Back").ToArray();
                    var ocrText2 = type1backpage.Where(z => z.Name.Equals("MRZ", StringComparison.OrdinalIgnoreCase)).Select(z => z.Text).FirstOrDefault();

                    if (new Regex(@"\d{13}").IsMatch(ocrText) || new Regex(@"\d{13}").IsMatch(ocrText2.Split('\n')[0].ToString().Substring((ocrText2.Split('\n')[0].ToString().IndexOf("«") == -1 ? 0 : ocrText2.Split('\n')[0].ToString().IndexOf("«")) + 1, ocrText2.Split('\n')[0].ToString().IndexOf("«") == -1 ? 0 : 13).Trim().FixDigits()))
                    {
                        BackPage.FormTemplateName = new Regex(@"\d{13}").IsMatch(ocrText2) ? "Type1_Back" : "Type4_Back";
                        BackPage.FileName = imageFile;
                        var folioZone = new OcrZone { Text = new Regex(@"\d{13}").IsMatch(ocrText) ? ocrText : ocrText2.Split('\n')[0].ToString().Substring(ocrText2.Split('\n')[0].ToString().IndexOf("«") + 1, 13).Trim().FixDigits(), IsMatch = true, Name = "Folio", ZoneConfidence = 100 };
                        //var cic
                        BackPage.OcrZones = new[] { folioZone };
                        Folio = folioZone.Text;
                        OCRProcessor.matchingtemplate = true;
                        SetPostProcessingPage(lastPageResults, "back",true,BackPage.FormTemplateName, BackPage.FileName, new Regex(@"\d{13}").IsMatch(ocrText2) ? type1backpage : type4backpage);
                    }
                    else
                    {
                        //ISFRONTPAGE
                        if (VotingCardOcr.generalPage != null || !string.IsNullOrEmpty(VotingCardOcr.generalPage))
                        {
                            if (VotingCardOcr.generalPage.Contains("FEDERAL") &&
                                VotingCardOcr.generalPage.Contains("CURP") &&
                                !VotingCardOcr.generalPage.Contains("NACIMIENTO"))
                            {
                                var templatename = "Type4_Front";
                                var type4frontpage = TemplatesWithFilterPlus
                                    .ForceApplyTemplate(imageFile, Templates.First(), templatename).ToArray();
                                
                                OCRProcessor.matchingtemplate = true;

                                SetFrontPageData(type4frontpage, true);
                                SetPostProcessingPage(lastPageResults, "front", true, templatename, imageFile, type4frontpage);
                                type4frontpage.ToList().ForEach(i => i.Text = i.Text + "?");
                                FirstPage = new OcrPage(genPage, templatename, 0, imageFile)
                                    {OcrZones = type4frontpage};
                            }

                            if (VotingCardOcr.generalPage.Contains("NACIMIENTO") && VotingCardOcr.generalPage.Contains("FEDERAL"))
                            {
                                var templatename = "Type3_Front";
                                var type3frontpage = TemplatesWithFilterPlus
                                    .ForceApplyTemplate(imageFile, Templates.First(), templatename).ToArray();
                                SetFrontPageData(type3frontpage, true);
                                SetPostProcessingPage(lastPageResults, "front", true, templatename, imageFile, type3frontpage);
                                OCRProcessor.matchingtemplate = true;
                                type3frontpage.ToList().ForEach(i => i.Text = i.Text + "?");
                                FirstPage = new OcrPage(genPage, templatename, 0, imageFile)
                                    {OcrZones = type3frontpage};
                            }

                            if (!VotingCardOcr.generalPage.Contains("NACIMIENTO") &&  VotingCardOcr.generalPage.Contains("FEDERAL") && !VotingCardOcr.generalPage.Contains("CURP"))
                            {
                                var templatename = "Type2_Front";
                                var type2frontpage = TemplatesWithFilterPlus.ForceApplyTemplate(imageFile, Templates.First(), templatename).ToArray();

                                
                               
                                type2frontpage.ToList().ForEach(i => i.Text = i.Text + "?");

                                FirstPage = new OcrPage(genPage, templatename, 0, imageFile)
                                    {OcrZones = type2frontpage};

                               
                                SetPostProcessingPage(lastPageResults, "front", true, templatename, imageFile, type2frontpage);
                                OCRProcessor.matchingtemplate = true;
                            }

                            if (VotingCardOcr.generalPage.Contains("NACIMIENTO") && VotingCardOcr.generalPage.Contains("NACIONAL"))
                            {
                                var templatename = "Type1_Front";
                                var type1frontpage = TemplatesWithFilterPlus
                                    .ForceApplyTemplate(imageFile, Templates.First(), templatename).ToArray();
                                
                                FirstPage = new OcrPage(genPage, templatename, 0, imageFile)
                                    {OcrZones = type1frontpage};


                                SetPostProcessingPage(lastPageResults, "front", true, templatename, imageFile, type1frontpage);
                                OCRProcessor.matchingtemplate = true;
                            }
                        }
                    }
                }

            }

            Weight = BackPageWeight(BackPage) + FirstPageWeight(FirstPage);
            var idZones = FirstPage.OcrZones.Union(BackPage.OcrZones).ToList();
            ConfidenceLevel = (int)(idZones.Sum(z => z.ZoneConfidence) / idZones.Count());


            if (OcrResults.All(p => p.FormTemplateName.Equals("No matching template")) && OCRProcessor.matchingtemplate !=true)
            {
                OCRProcessor.matchingtemplate = false;
            }

        }

        private void SetPostProcessingPage(OcrPage pageresults, string frontorback, bool shouldaddquestionmark = false,
            string backPageFormTemplateName = "", string fileName = "", OcrZone[] forcedOcrZones = null)
        {
            var pagina = new Xml.Page();

            pagina = new CrRNRBAZDIGOCRPostProcessing.Models.Xml.Page();
            pagina.DatosEngineOcr.OcrZones = pageresults!= null ? pageresults.OcrZones.Select(z => new Xml.OcrDataZone()
            {
                Name = z.Name,
                ZoneConfidence = z.ZoneConfidence.ToString(),
                ZoneType = z.ZoneType.ToString(),
                BoundingRect = z.BoundingRect.ToString(),
                FillingMethod = z.FillingMethod.ToString(),
                Filter = "Filtro: " + z.Filter.ToString() + " FilterPlus: " + z.FilterPlus,
                IsMatch = z.IsMatch.ToString(),
                RecognitionModule = z.RecognitionModule.ToString(),
                RegexPattern = z.RegexPattern
            }).ToList() : forcedOcrZones.Select(z => new Xml.OcrDataZone()
                {
                    Name = z.Name,
                    ZoneConfidence = z.ZoneConfidence.ToString(),
                    ZoneType = z.ZoneType.ToString(),
                    BoundingRect = z.BoundingRect.ToString(),
                    FillingMethod = z.FillingMethod.ToString(),
                    Filter = "Filtro: " + z.Filter.ToString() + " FilterPlus: " + z.FilterPlus,
                    IsMatch = z.IsMatch.ToString(),
                    RecognitionModule = z.RecognitionModule.ToString(),
                    RegexPattern = z.RegexPattern
                }).ToList();

            pagina.DatosEngineOcr.BitsPerPixel = pageresults != null  ? pageresults.BitsPerPixel.ToString() : "0";
            pagina.DatosEngineOcr.Dpi = pageresults != null ? pageresults.Dpi.ToString() : "300" ;
            pagina.DatosEngineOcr.FormTemplateName = pageresults != null ? pageresults.FormTemplateName : backPageFormTemplateName;
            pagina.DatosEngineOcr.Languages = pageresults != null ?  pageresults.Languages.ToString() :  "";
            pagina.DatosEngineOcr.Size = pageresults != null ? pageresults.Size.ToString() : "";
            pagina.DatosEngineOcr.FrontFileName = pageresults != null  ? pageresults.FileName : fileName;

            if (frontorback == "front")
            {
                paginafrontal = pagina;
                
                paginafrontal = XmlResult.GenerateFrontPage(Nombre + (shouldaddquestionmark ? "?" : "") ?? "", Domiciolio + (shouldaddquestionmark ? "?" : "") ?? "", Curp + (shouldaddquestionmark ? "?" : "") ?? "",
                    ClaveElector + (shouldaddquestionmark ? "?" : "") ?? "",
                    FolioFrente + (shouldaddquestionmark ? "?" : "") ?? "", AnoRegistro + (shouldaddquestionmark ? "?" : "") ?? "", NumeroEmision + (shouldaddquestionmark ? "?" : "") ?? "", EstadoId + (shouldaddquestionmark ? "?" : "") ?? "", MunicipioId + (shouldaddquestionmark ? "?" : "") ?? "",
                    Localidad + (shouldaddquestionmark ? "?" : "") ?? "",
                    Seccion + (shouldaddquestionmark ? "?" : "") ?? "", AnoEmision + (shouldaddquestionmark ? "?" : "") ?? "", Vigencia + (shouldaddquestionmark ? "?" : "") ?? "", Sexo + (shouldaddquestionmark ? "?" : "") ?? "",OCRProcessor.shouldlog,OCRProcessor.shouldlogtodb, OCRProcessor.typeofpostprocessing, OCRProcessor.scoreforelastic,OCRProcessor.levenshteinforredis);
 }

            //paginafrontal.DatosEngineOcr.OcrZones = FirstPage.OcrZones.ToList();
            
            else
            {
                paginatrasera = pagina;
                paginatrasera = XmlResult.GenerateBackPage(folioZone != null ? folioZone.Text : Folio, identityCard != null ? identityCard.DocumentNumber: "", OCRProcessor.shouldlog,OCRProcessor.shouldlogtodb, OCRProcessor.typeofpostprocessing, OCRProcessor.scoreforelastic, OCRProcessor.levenshteinforredis);
                paginatrasera.DatosEngineOcr.BitsPerPixel = pageresults != null ? pageresults.BitsPerPixel.ToString() : "0";
                paginatrasera.DatosEngineOcr.Dpi = pageresults != null ? pageresults.Dpi.ToString() : "300";
                paginatrasera.DatosEngineOcr.FormTemplateName = pageresults != null ? pageresults.FormTemplateName : backPageFormTemplateName;
                paginatrasera.DatosEngineOcr.Languages = pageresults != null ? pageresults.Languages.ToString() : "";
                paginatrasera.DatosEngineOcr.Size =pageresults != null ? pageresults.Size.ToString() : "";
                paginatrasera.DatosEngineOcr.FrontFileName = pageresults != null ? pageresults.FileName : fileName;
            }
        }
       
       
        
        
        

        private void SetFrontPageData(OcrZone[] typefrontpage, bool shouldaddquestionmark = false)
         {
            if (typefrontpage != null)
            {
                MunicipioId = typefrontpage.FirstOrDefault(z => z.Name.Equals("MunicipioId", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : "");
                EstadoId = typefrontpage.FirstOrDefault(z => z.Name.Equals("EstadoId", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : "");
                Domiciolio = typefrontpage.FirstOrDefault(z => z.Name.Equals("Domicilio", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : "");;
                Curp = typefrontpage.FirstOrDefault(z => z.Name.Equals("Curp", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Curp", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                Nombre = typefrontpage.FirstOrDefault(z => z.Name.Equals("Nombre", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Nombre", StringComparison.InvariantCultureIgnoreCase)).Text.Replace("NOMBRE", "").Replace("DOMICILIO", "") + (shouldaddquestionmark ? "?" : ""); ;
                ClaveElector = typefrontpage.FirstOrDefault(z => z.Name.Equals("ClaveElector", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("ClaveElector", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                FolioFrente = typefrontpage.FirstOrDefault(z => z.Name.Equals("FolioFrente", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("FolioFrente", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                AnoRegistro = typefrontpage.FirstOrDefault(z => z.Name.Equals("AnoRegistro", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("AnoRegistro", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                NumeroEmision = typefrontpage.FirstOrDefault(z => z.Name.Equals("NumeroEmision", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("NumeroEmision", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                Localidad = typefrontpage.FirstOrDefault(z => z.Name.Equals("Localidad", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Localidad", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                Seccion = typefrontpage.FirstOrDefault(z => z.Name.Equals("Seccion", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Seccion", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                AnoEmision = typefrontpage.FirstOrDefault(z => z.Name.Equals("AnoEmision", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("AnoEmision", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                Vigencia = typefrontpage.FirstOrDefault(z => z.Name.Equals("Vigencia", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Vigencia", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                Sexo = typefrontpage.FirstOrDefault(z => z.Name.Equals("Sexo", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Sexo", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                //FOR ABSENCE OF STREET NAMES OR ONE OF THE SURNAMES
                ApellidoPaternoZone = typefrontpage.FirstOrDefault(z => z.Name.Equals("ApellidoPaterno", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("ApellidoPaterno", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                ApellidoMaternoZone = typefrontpage.FirstOrDefault(z => z.Name.Equals("ApellidoMaterno", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("ApellidoMaterno", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                CalleZone = typefrontpage.FirstOrDefault(z => z.Name.Equals("Calle", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Calle", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : ""); ;
                ColoniaZone = typefrontpage.FirstOrDefault(z => z.Name.Equals("Colonia", StringComparison.InvariantCultureIgnoreCase)) == null ? "" : typefrontpage.First(z => z.Name.Equals("Colonia", StringComparison.InvariantCultureIgnoreCase)).Text + (shouldaddquestionmark ? "?" : "");
            }
            


        }

        public void ValidateScannedDocument(string idtype)
        {

            List<KeyValuePair<string, string[]>> kvpidtemplate = new List<KeyValuePair<string, string[]>>();


            kvpidtemplate.Add(new KeyValuePair<string, string[]>("Pasaporte", new string[] { "Type1_Passport", "Type2_Passport", "Type3_Passport" , "Type4_Passport" }));
            kvpidtemplate.Add(new KeyValuePair<string, string[]>("Credencial IFE", new string[] { "Type1_Back", "Type1_Front", "Type2_Front", "Type3_Front", "Type4_Back", "Type4_Front", "Type5_Front", "Full OCR Backpage" }));


            if (idtype == "Credencial IFE")
            {

                if (!(kvpidtemplate.Find(z => z.Key == idtype).Value.Any(x => x.Equals(FirstPage.FormTemplateName ?? BackPage.FormTemplateName ?? ""))))
                {
                    OCRProcessor.matchingdocument = false;
                }
                else
                {
                    OCRProcessor.matchingdocument = true;
                }

            }
        }

        private static double FirstPageWeight(OcrPage page)
        {
            Console.WriteLine(page.FormTemplateName);
            //GetZone(page, "Nombre").Text.Contains("?")
           
            if (page.FormTemplateName == "Type4_Front")
            {
                if (GetZone(page, "Domicilio").Text.Contains("DOMICILIO") || GetZone(page, "Domicilio").Text.Contains("AÑO") || GetZone(page, "Domicilio").Text.Contains("ANO"))
                {
                    GetZone(page, "Domicilio").ZoneConfidence = GetZone(page, "Domicilio").ZoneConfidence * .1;
                }

                if (GetZone(page, "CURP").ZoneConfidence == 100)
                {
                    GetZone(page, "ClaveElector").ZoneConfidence = 100;
                }

                if (GetZone(page, "Domicilio").Text.Split('\n')[0].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length < 4)
                {
                    GetZone(page, "Domicilio").ZoneConfidence *= .1;
                }

                //Console.WriteLine("Peso zona Nombre: " + (GetZone(page, "Nombre").ZoneConfidence * 2.5));
                //Console.WriteLine("Peso zona Domicilio: " + (GetZone(page, "Domicilio").ZoneConfidence * 2.5));
                //Console.WriteLine("Peso zona CURP: " + (GetZone(page, "CURP").ZoneConfidence * 2.5));
                //Console.WriteLine("Peso zona ClaveElector: " + (GetZone(page, "ClaveElector").ZoneConfidence * 2.5));
                //Console.WriteLine("Peso zona EstadoId: " + (GetZone(page, "EstadoId").ZoneConfidence * 2.0));
                //Console.WriteLine("Peso zona MunicipioId: " + (GetZone(page, "MunicipioId").ZoneConfidence * 2.0));
                //Console.WriteLine("Añadido por cantidad zonas: " + (page.OcrZones.Length == 14 ? 10 : 0));
                
                var total = (GetZone(page, "Nombre").ZoneConfidence * 2.5) + (GetZone(page, "Domicilio").ZoneConfidence * 2.5) + ((GetZone(page, "CURP").ZoneConfidence) * 2.5) + ((GetZone(page, "ClaveElector").ZoneConfidence) * 2.5) + ((GetZone(page, "EstadoId").ZoneConfidence) * 2.0) + ((GetZone(page, "MunicipioId").ZoneConfidence * 2.0)) + (page.OcrZones.Length == 14 ? 10 : 0);
        
                //Console.WriteLine("Peso total zona: " + total);

                return total;


            }

            //if (page.FormTemplateName == "Type2_Front")
            //{
            //   page.
            //    return GetZone(page, "Nombre").ZoneConfidence * 2.5 +
            //           GetZone(page, "Domicilio").ZoneConfidence * 2.5 +
            //           (GetZone(page, "ClaveElector").ZoneConfidence) * 2.5 +
            //           (GetZone(page, "EstadoId").ZoneConfidence) * 2.0 +
            //           (GetZone(page, "MunicipioId").ZoneConfidence * 2.0) + page.OcrZones.Length == 11 ? 10 : 0;
            //}

            return GetZone(page, "Nombre").ZoneConfidence * 2.5 +
                   GetZone(page, "Domicilio").ZoneConfidence * 2.5 +
                   (GetZone(page, "ClaveElector").ZoneConfidence) * 2.5 +
                   (GetZone(page, "EstadoId").ZoneConfidence) * 2.0 +
                   (GetZone(page, "MunicipioId").ZoneConfidence * 2.0);



        }

        private static double BackPageWeight(OcrPage page)
        {
            if (page.FormTemplateName == "Type4_Back" || page.FormTemplateName == "Type2_Back")
            {
                return (GetZone(page, "Folio").ZoneConfidence);
            }
            else
            {
                if (Regex.Match((GetZone(page, "MRZ").Text) ?? "", @"\d{13}").Success && Regex.Match((GetZone(page, "MRZ").Text) ?? "", @"IDMEX").Success)
                {
                    GetZone(page, "MRZ").ZoneConfidence *= 1.5;
                }
                return (GetZone(page, "MRZ").ZoneConfidence);
            }
        }



        private static OcrZone GetZone(OcrPage page, string zoneName)
        {
            return page.OcrZones.FirstOrDefault(z => z.Name.Equals(zoneName, StringComparison.InvariantCultureIgnoreCase)) ?? new OcrZone();
        }

        private IEnumerable<OcrPackage> GetOcrPackages()
        {
            return from setting in SettingProfiles
                   from file in Images
                   from template in Templates
                   select new OcrPackage
                   {
                       FileName = file,
                       Settings = setting,
                       Template = template
                   };
        }


        [XmlIgnore]
        public SettingCollection[] SettingProfiles { get; set; }
        [XmlIgnore]
        public string NombreCompleto { get; set; }
        [XmlIgnore]
        public string Domiciolio { get; set; }
        [XmlIgnore]
        public string DomicilioNoReplace { get; set; }
        [XmlIgnore]
        public string[] Templates { get; }
        [XmlIgnore]
        public OcrPage[] OcrResults { get; private set; }

        [XmlIgnore]
        public string ApellidoPaternoZone { get; set; }
        [XmlIgnore]
        public string ApellidoMaternoZone { get; set; }
        [XmlIgnore]
        public string CalleZone { get; set; }
        [XmlIgnore]
        public string ColoniaZone { get; set; }

        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Calle { get; set; }
        public string Colonia { get; set; }
        public string Delegacion { get; set; }
        public string FolioFrente { get; set; }
        public string ClaveElector { get; set; }
        public string Curp { get; set; }
        public string Seccion { get; set; }
        public string Localidad { get; set; }
        public string AnoEmision { get; set; }
        public string Vigencia { get; set; }
        public string Sexo { get; set; }
        public string AnoRegistro { get; set; }
        public string NumeroEmision { get; set; }
        public string Folio { get; set; }
        public int ConfidenceLevel { get; set; }
        public double Weight { get; set; }
        public string[] Images { get; }
        public long ElapsedTime { get; set; }
        public OcrPage FirstPage { get; set; } = new OcrPage();
        public OcrPage BackPage { get; set; } = new OcrPage();
        public string MunicipioId { get; set; }
        public string EstadoId { get; set; }
        public bool matchingdocument { get; set; }
        public Xml.Page paginafrontal { get; set; }
        public OcrZone folioZone { get; private set; }
        public Xml.Page paginatrasera { get; set; }
        public static String generalPage { get; set; }
        public static Page genPage { get; set; }


        /// <summary>
        /// Checks of the matched template was a front page template
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static bool IsFrontPage(OcrPage page)
        {
            return page.FormTemplateName.EndsWith("Front", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Checks if the template was back page template
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static bool IsBackPage(OcrPage page)
        {
            return page.FormTemplateName.EndsWith("Back", StringComparison.InvariantCultureIgnoreCase);
        }

        public override string ToString()
        {
            return $"Nombre: {Nombre}\nDomicilio: {Domiciolio}\nCurp:{Curp}\nFolio: {Folio}\nElapsed:{ElapsedTime / 1000.0:0.00}";
        }

        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            var name = Nombre?.Replace("\n", string.Empty);
            var address = Domiciolio?.Replace("\n", string.Empty);

            Console.WriteLine($"        Nombre: {name}");
            Console.WriteLine($"     Domicilio: {address}");
            Console.WriteLine($"          Curp: {Curp}");
            Console.WriteLine($"         Folio: {Folio}");
            Console.WriteLine($"    Confidence: {ConfidenceLevel}% / {Weight}");

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
