﻿using System.Drawing;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class Word
    {
        /// <summary>
        /// Gets or sets the word text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the word coordinates
        /// </summary>
        public Rectangle Rectangle { get; set; }


        /// <summary>
        /// Gets or sets the word alternatives
        /// </summary>
        public string[] Alternatives { get; set; }
    }
}
