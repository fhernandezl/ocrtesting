﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nuance.OmniPage.CSDK.ArgTypes;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class ZoneSettings
    {
        public ZoneSettings()
        {
            Enabled = false;
            FilterPlus = string.Empty;
            RegexPattern = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RegexPattern { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ZONETYPE ZoneType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public CHR_FILTER ZoneFilter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FilterPlus { get; set; }
    }
}
