﻿using Nuance.OmniPage.CSDK.Objects;

namespace Nuance.Omnipage.SampleCode.Types
{
    public class OcrPackage
    {

        public string FileName { get; set; }

        public SettingCollection Settings { get; set; } = new SettingCollection();

        public string Template { get; set; }

        public bool UseJsonOverrides { get; set; } = true;
    }
}
