﻿using Nuance.Omnipage.SampleCode.Types;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrRNRBAZDIGOCRPostProcessing;
using CrRNRBAZDIGOCRPostProcessing.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;
using OcrPage = Nuance.Omnipage.SampleCode.Types.OcrPage;
using System.Xml;


namespace Com.BancoAzteca.OCR
{
    public class OCRProcessor
    {
        private static VotingCardOcr idProcess = null;
        public static bool Shouldreplace = true;
        public static bool shouldlog = true;
        public static bool shouldlogtodb = true;
        internal static int typeofpostprocessing = 0;
        internal static double scoreforelastic = 7;
        internal static int levenshteinforredis = 0;
        private const string Template = @"K:\imxtrb\OCR\Plantillas OCR v20\DocumentTemplates.ftl";

        public void CreateXMLToUpload(string filepath)
        {

            Shouldreplace = false;
            shouldlog = false;
            shouldlogtodb = false;

         //string []inputFiles = Directory.EnumerateFiles(filepath).Where(path => Regex.IsMatch(path, @"Imagen[6-9]{1}[0-9]{0,1}[0-9]{0,1}_[1|2]\.jpg$")).ToArray();
           var inputFiles = Directory.EnumerateFiles(filepath, "*.jpg").ToArray();
            
            // Set images
            var images = inputFiles.GroupBy(f =>
            {
                var name = Path.GetFileNameWithoutExtension(f);
                return name?.Substring(0, name.Length);
            });

            foreach (var idScan in images)
            {
                InitEngine();

                Console.WriteLine($"\nProcessing {idScan.Key} - {idScan.First()}\n{new string('-', 120)}\n");

                var idImages = idScan.ToArray();



                var idProcess = new VotingCardOcr(idImages, new[] { Template })
                {
                    SettingProfiles = GetSettingsPackage()
                };


                idProcess.Process();
                idProcess.ExtractData(); 
                idProcess.Display(); 


                if (idProcess.Folio != null && !idProcess.Folio.Contains("ERALDEELECTORES"))
                {
                    File.WriteAllText(Path.ChangeExtension(idScan.First(), ".xml"), idProcess.BackPage?.ToXml());
                }
                else
                {
                    if (idProcess.Folio is null)
                        File.WriteAllText(Path.ChangeExtension(idScan.First(), ".xml"), idProcess.FirstPage?.ToXml());
                }


                QuitEngine();
            }
            //RECERR

            Console.WriteLine("\n\nPress ENTER to exit.");
            Console.ReadLine();
        }

       

        public Xml.Page DoExtractFromImage(string image)
        {
                Shouldreplace = true;
                var idScan = Path.GetFileNameWithoutExtension(image);
                idScan = idScan?.Substring(0, idScan.Length);

                InitEngine();

                Console.WriteLine($"\nProcessing {idScan} - {idScan.First()}\n{new string('-', 120)}\n");

                 idProcess = new VotingCardOcr(new string[]{ image }, new[] { Template })
                {
                    SettingProfiles = GetSettingsPackage()
                };


                idProcess.Process();
                idProcess.ExtractData();
                idProcess.Display();


              

                if (idProcess.Folio != null && !idProcess.Folio.Contains("ERALDEELECTORES"))
                {
                    
                    return idProcess.paginatrasera;
                    
                }
                else if(idProcess.Folio is null)
                {
                        return idProcess.paginafrontal;
                }
                else
                {
                    var idProcesspassport = new PassportOcr(new string[] { image }, new[] { Template })
                    {
                        SettingProfiles = null
                    };

                    idProcesspassport.ExtractDataFromFullPageOCR();

                    if (idProcesspassport.paginaXml != null)
                    {
                        return idProcesspassport.paginaXml;
                    }
                }
                QuitEngine();


            Console.WriteLine("\n\nPress ENTER to exit.");
            Console.ReadLine();
           

            return null;
        }

        public string ExtractXmlFromImage(string file1, string file2, string idtype)
        {

            try
            {

                if (!File.Exists(file1) && !File.Exists(file2)) 
                    return "4";
                if (!File.Exists(file2) && idtype == Constants.DocumentNames.CredencialIfe) 
                    return "4";

                //@"C:\Users\fahernandezl\Documents\ocrtest\800 Imagenes\Whole\";
               
                //string []inputFiles = Directory.EnumerateFiles(filePath).Where(path => Regex.IsMatch(path, @"Imagen[7-9]{1}[0-9]{0,1}[0-9]{0,1}_[1|2]\.jpg$")).ToArray();

                var inputFiles = !File.Exists(file2) ? new String[] { file1 } : new String[] { file1 , file2};
             

                // Set images
                var images = inputFiles.GroupBy(f =>
                {
                    var name = Path.GetFileNameWithoutExtension(f);
                    return name?.Substring(0, name.Length);
                });


                Xml.Page backpage = null;
                Xml.Page frontpage = null;
                string stringBackPage = string.Empty;
                string stringFrontPage = string.Empty;

                foreach (var idScan in images)
                {
                    if (InitEngine())
                    {
                        
                        Console.WriteLine($"\nProcessing {idScan.Key} - {idScan.First()}\n{new string('-', 120)}\n");

                        var idImages = idScan.ToArray();

                        if (idtype == Constants.DocumentNames.CredencialIfe)
                        {
                            var idProcess = new VotingCardOcr(idImages, new[] { Template })
                            {
                                SettingProfiles = GetSettingsPackage()
                            };

                            idProcess.Process();

                            idProcess.ExtractData();

                            if (idtype != null && idtype != String.Empty)
                            {

                                idProcess.ValidateScannedDocument(idtype);
                                //appLog.WriteEntry("Valido documento correctamente");

                                bool macheo = matchingdocument;

                                if (!macheo) { return "4"; }
                                //else { return "4"; }

                                //if (!idProcess.matchingdocument)
                                //{
                                //    return "4";
                                //}

                            }
                            bool matchingtemplatee = matchingtemplate;

                            if (!matchingtemplatee)
                            {
                                return "3";
                            }



                            if (idProcess.BackPage.OcrZones.Length > 0 && backpage == null)
                            {
                                backpage = idProcess.paginatrasera;
                            }


                            if (idProcess.FirstPage.OcrZones.Length > 0 && frontpage == null)
                            {
                                frontpage = idProcess.paginafrontal;
                            }


                            QuitEngine();

                            Xml.Country country = new Xml.Country();
                            Xml.Countries countries = new Xml.Countries();
                            country.Name = "MEXICO";
                            countries.Country = country;
                            country.Page = new List<Xml.Page>() { frontpage, backpage };
                            if (frontpage != null && backpage != null)

                                return countries.ToXml().ToString();
                        }
                        else if (idtype == Constants.DocumentNames.Pasaporte)
                        {
                            var idProcess = new PassportOcr(idImages, new[] { Template })
                            {
                                SettingProfiles = null
                            };

                            idProcess.ExtractDataFromFullPageOCR();

                            if (idProcess.paginaXml != null)
                            {
                                backpage = idProcess.paginaXml;
                            }
                            //try
                            //{
                            //    var idProcess = new PassportOcr(idImages, new[] { template })
                            //    {
                            //        SettingProfiles = GetSettingsPackageForPassport()
                            //    };

                            //    idProcess.Process();

                            //    idProcess.ExtractData();

                            //    if (idtype != null && idtype != String.Empty)
                            //    {
                            //        idProcess.ValidateScannedDocument(idtype);

                            //        bool macheo = matchingdocument;

                            //        if (!macheo) { return "4"; }
                            //    }

                            //    bool matchingtemplatee = matchingtemplate;

                            //    if (!matchingtemplatee)
                            //    {
                            //        return "3";
                            //    }

                            //    if (idProcess.BackPage.OcrZones.Length > 0 && backpage == null)
                            //    {
                            //        backpage = idProcess.paginaXml;
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    var idProcess = new PassportOcr(idImages, new[] { template })
                            //    {
                            //        SettingProfiles = GetSettingsPackageForPassport()
                            //    };

                            //    idProcess.ExtractDataFromFullPageOCR();

                            //    if (idProcess.paginaXml != null)
                            //    {
                            //        backpage = idProcess.paginaXml;
                            //    }
                            //    clsLog.ArchiveLog(ex.Message);
                            //    clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", ex.Message);
                            //}

                            QuitEngine();
                            clsLog.ArchiveLog("Se apagó el engine en flujo de pasaporte.");
                            clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", "Se apagó el engine en flujo de pasaporte.");

                            Xml.Country country = new Xml.Country();
                            Xml.Countries countries = new Xml.Countries();
                            country.Name = IdentityDocumentBase.GetCountryByCode(backpage.PaisExpedicion);
                            countries.Country = country;
                            country.Page = new List<Xml.Page>() { backpage };
                            if (backpage != null)
                                return countries.ToXml().ToString();
                        }
                        else {
                            if (File.Exists(file2) && File.Exists(file1)) {
                                var idProcess = new Types.GenericDocumentOcr(idImages, new[] { Template })
                                {
                                    SettingProfiles = GetSettingsPackage()
                                };

                                idProcess.Process();
                                idProcess.ExtractData();

                                bool matchingtemplatee = idProcess.IsTemplateValid();

                                if (!matchingtemplatee)
                                {
                                    return "3";
                                }

                                if (idtype != null && idtype != String.Empty)
                                {
                                    bool macheo = idProcess.IsImageDocumentTheSameAsIdDocument();//idProcess.ValidateExtractionByOcrZonesQuantity();

                                    if (!macheo) { return "4"; }
                                }

                                if (idProcess.FinishedBackPageOperations) {
                                    backPageMatching = true;
                                    BackPageZones = idProcess.BackPageZonesAndValues;
                                    backPageTemplateName = idProcess.BackTemplateName;
                                }
                                if (idProcess.FinishedFrontPageOperations)
                                {
                                    frontPageMatching = true;
                                    FrontPageZones = idProcess.FrontPageZonesAndValues;
                                    frontPageTemplateName = idProcess.FrontTemplateName;
                                }

                                QuitEngine();

                                if (frontPageMatching && backPageMatching) {
                                    return idProcess.GetFinalXml(FrontPageZones, frontPageTemplateName, BackPageZones, backPageTemplateName);
                                }
                            }
                            else {
                                var idProcess = new Types.GenericDocumentOcr(idImages, new[] { Template })
                                {
                                    SettingProfiles = GetSettingsPackage()
                                };

                                idProcess.Process();
                                idProcess.ExtractData();

                                bool matchingtemplatee = idProcess.IsTemplateValid();

                                if (!matchingtemplatee)
                                {
                                    return "3";
                                }

                                if (idtype != null && idtype != String.Empty)
                                {
                                    bool macheo = idProcess.IsImageDocumentTheSameAsIdDocument();//idProcess.ValidateExtractionByOcrZonesQuantity();

                                    if (!macheo) { return "4"; }
                                }

                                if (idProcess.FinishedFrontPageOperations)
                                {
                                    frontPageMatching = true;
                                    FrontPageZones = idProcess.FrontPageZonesAndValues;
                                    frontPageTemplateName = idProcess.FrontTemplateName;
                                }

                                QuitEngine();

                                if (idProcess.FinishedFrontPageOperations)
                                {
                                    return idProcess.GetFinalXml(FrontPageZones, frontPageTemplateName, null, null);
                                }
                            }
                        }

                    }
                    else
                    {
                        return "1";
                    }
                }

                if (backpage != null && frontpage == null)
                {
                    return "5";
                }

                if (frontpage != null && backpage == null)
                {
                    return "5";
                }
            }
            catch (Exception ex)
            {

                //var appLog = new EventLog("TemplateMatching");
                //appLog.Source = "BibliotecaMatcheoTemplates";
                //appLog.WriteEntry("Excepcion OCR tipo 2" + e);
                //return "2, exception: " + e.Message + ";\n stacktrace: " + e.StackTrace;

                clsLog.ArchiveLog("Regresa error 2 debido a: " + ex.Message);
                clsLog.BDLog(0, "OCRProcessor", "ExtractXmlFromImage", "Regresa error 2 debido a: " + ex.Message);
                return "2";
            }

            return "5";

        }
        
        public SettingCollection[] GetSettingsPackage()
        {


            return Directory.EnumerateFiles(@".\settings\", "*.sts")
                .Select(sts =>
                {
                    var settings = new SettingCollection();
                    settings.Load(sts);

                    return new SettingCollection(settings);
                })
                .ToArray();
        }

        //public static SettingCollection[] GetSettingsPackage() => new[]

        //              {
        //                new SettingCollection(new SettingCollection{
        //                    ImageResolutionEnhancement = IMG_RESENH.RE_STANDARD,
        //                    ImageDeskew = IMG_DESKEW.DSK_NO,
        //                    DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
        //                    RMTradeoff = RMTRADEOFF.TO_ACCURATE,
        //                     }),
        //                new SettingCollection(new SettingCollection
        //                {
        //                    ImageResolutionEnhancement = IMG_RESENH.RE_AUTO,
        //                    ImageDeskew = IMG_DESKEW.DSK_NO,
        //                    DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
        //                    RMTradeoff = RMTRADEOFF.TO_ACCURATE,
        //                }),
        //                new SettingCollection(new SettingCollection{
        //                    ImageResolutionEnhancement = IMG_RESENH.RE_YES,
        //                    ImageDeskew = IMG_DESKEW.DSK_NO,
        //                    DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
        //                    RMTradeoff = RMTRADEOFF.TO_ACCURATE,
        //                 }),
        //                new SettingCollection(new SettingCollection{
        //                    ImageConversionMode = IMG_CONVERSION.CNV_GLOBAL,
        //                    ImageBrightness = 0,
        //                    ImageThreshold = 0,
        //                    ImageResolutionEnhancement = IMG_RESENH.RE_YES,
        //                    ImageDeskew = IMG_DESKEW.DSK_NO,
        //                    DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
        //                    RMTradeoff = RMTRADEOFF.TO_ACCURATE,
        //                 }),
        //                new SettingCollection(new SettingCollection{
        //                    ImageConversionMode = IMG_CONVERSION.CNV_SET,
        //                    ImageBrightness = 0,
        //                    ImageThreshold = 120,
        //                    ImageResolutionEnhancement = IMG_RESENH.RE_NO,
        //                    DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
        //                    RMTradeoff = RMTRADEOFF.TO_ACCURATE,
        //                 })
        //               };


        public static SettingCollection[] GetSettingsPackageForPassport() => new[]
                      {
                        new SettingCollection(new SettingCollection{
                            DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W,
                            DefaultFillingMethod = FILLINGMETHOD.FM_OCRB
                             })
                       };

        public static bool InitEngine()
        {
            try
            {
                const string csdk203 = @"C:\EnginePlusv20";
                Engine.SetLicenseKey(@"C:\EnginePlusv20\NuanceOmniPageCaptureSDK_20.0.lcxz", "AV-8p-Kyv-Rf");
                Engine.Init(null, null, true, csdk203);
                clsLog.ArchiveLog("INICIO el Engine de OCR de forma correcta");
                clsLog.BDLog(0, "OCRProcessor", "InitEngine", "INICIO el Engine de OCR de forma correcta");
                return true;
            } 
            catch (Exception ex)
            {
                clsLog.ArchiveLog("Falló al iniciar el engine debido a: " + ex.Message);
                clsLog.BDLog(0, "OCRProcessor", "InitEngine", "Falló al iniciar el engine debido a: " + ex.Message);
                return false;
            }
        }

        public static void QuitEngine()
        {
            Engine.ForceQuit();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            idProcess = null;
            clsLog.ArchiveLog("TERMINO el Engine de OCR de forma correcta");
            clsLog.BDLog(0, "OCRProcessor", "QuitEngine", "TERMINO el Engine de OCR de forma correcta");
        }
        public static bool matchingtemplate { get; set; }
        public static bool matchingdocument { get; set; }

        public static bool frontPageMatching { get; set; }
        public static bool backPageMatching { get; set; }

        public static string frontPageTemplateName { get; set; }
        public static string backPageTemplateName { get; set; }

        public static Types.XmlResponse.XmlPage FrontPage { get; set; }
        public static Types.XmlResponse.XmlPage BackPage { get; set; }

        public List<KeyValuePair<string, string>> FrontPageZones { get; set; }
        public List<KeyValuePair<string, string>> BackPageZones { get; set; }
    }

}
