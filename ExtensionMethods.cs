﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Com.BancoAzteca.OCR;
using Newtonsoft.Json;
using Nuance.Omnipage.SampleCode.Types;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;

namespace Nuance.Omnipage.SampleCode
{
    /// <summary>
    /// The extension methods.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Gets the recognized text
        /// </summary>
        /// <param name="page">the page instance.</param>
        /// <returns>the OCRed text of the page</returns>
        public static string RecognizedText(this Page page)
        {
            
            var zoneletters = page[IMAGEINDEX.II_CURRENT]
                        .GetLetters();

            //zoneletters.ToList().ForEach(i=> (i.err & 127) > 64 ? i.code = '?'  : i.code = i.code);
            if (OCRProcessor.Shouldreplace)
            {
                foreach (var letter in zoneletters)
                {
                    if ((letter.err & 127) > 64)
                    {
                        letter.code = '?';
                       
                    }
                }
            }
            
            
            return new string(zoneletters.FixSpaces(zoneletters.Any() ? zoneletters.Max(l => l.height) / 3 : 100).GetCharsUntilNoise()
                        .Select(c => c.makeup.HasFlag(MAKEUP.R_ENDOFLINE) ? new[] { c.code, '\n' } : new[] { c.code })
                        .SelectMany(x => x)
                        .ToArray());
        }

        public static string GetPageLetters(this Page page)
        {
            return new string(page[IMAGEINDEX.II_CURRENT].GetLetters().Select(c => c.makeup.HasFlag(MAKEUP.R_ENDOFLINE) ? new[] { c.code, '\n' } : new[] { c.code }).SelectMany(x => x).ToArray());

        }

        /// <summary>
        /// Gets the zone confidence level in percentage.
        /// </summary>
        /// <param name="zone">the ocr zone instance</param>
        /// <returns>a double value beteen 0 and 100</returns>
        public static double GetZoneConfidence(this OCRZone zone)
        {
            var zoneLetters = zone.Page[IMAGEINDEX.II_CURRENT].GetLetters().Where(letter => letter.zone == zone.Index).ToArray();
            return zoneLetters.Count(l => (l.err & 127) <= 64) * 100.0 / zoneLetters.Length;
        }

        public static string FixDigits(this string number)
        {
            return (number ?? string.Empty).Replace('O', '0').Replace('I', '1').Replace('B', '8').Replace('i', '1');
        }

        public static string FixLetters(this string text)
        {
            return (text ?? string.Empty).Replace('0', 'O').Replace('1', 'I').Replace('8', 'B').Replace('2', 'R');
        }

        //public static string GetZoneText(this OCRZone zone)
        //{
        //    return new string(zone.Page[IMAGEINDEX.II_CURRENT]
        //                           .GetLetters()
        //                           .Where(letter => letter.zone == zone.Index)
        //                           .Select(c => c.makeup.HasFlag(MAKEUP.R_ENDOFLINE) ? new[] { c.code, '\n' } : new[] { c.code })
        //                           .SelectMany(x => x)
        //                           .ToArray());
        //}

        public static string GetZoneText(this OCRZone zone, bool removeCharsAfterLongSpace = true)
        {
            IEnumerable<LETTER> zoneLetters = zone.Page[IMAGEINDEX.II_CURRENT]
                .GetLetters()
                .Where(letter => letter.zone == zone.Index);

            if (removeCharsAfterLongSpace)
            {
                return new string(zoneLetters.FixSpaces(zoneLetters.Any() ? zoneLetters.Max(l => l.height) / 3 : 100).GetCharsUntilNoise().Select(c => c.makeup.HasFlag(MAKEUP.R_ENDOFLINE) ? new[] { c.code, '\n' } : new[] { c.code }).SelectMany(x => x).ToArray());

            }
            else
            {
                return new string(zoneLetters
                    .Select(c => c.makeup.HasFlag(MAKEUP.R_ENDOFLINE) ? new[] { c.code, '\n' } : new[] { c.code })
                    .SelectMany(x => x)
                    .ToArray());
            }
        }

        /// <summary>Fixes space issues</summary>
        /// <param name="letters">an enumeration of LETTERs</param>
        /// <param name="spaceWidth">the minimal space width in pixels</param>
        /// <returns>an enumeration of LETTERs with injected spaces.</returns>
        public static IEnumerable<LETTER> FixSpaces(this IEnumerable<LETTER> letters, int spaceWidth)
        {
            if (!letters.Any())
            {
                yield break;
            }

            var current = letters.First();

            yield return current;

            foreach (var letter in letters.Skip(1))
            {
                if (current.code != ' ' && letter.code != ' ' && current.VerticalDistanceFrom(letter) > spaceWidth)
                {
                    // inject a dummy space if the distance is more than space width param value.
                    yield return new LETTER(letter) { code = ' ', left = (ushort)(letter.left + 5), width = 10 };
                }

                yield return letter;
                current = letter;
            }
        }

        public static IEnumerable<LETTER> GetCharsUntilNoise(this IEnumerable<LETTER> letters)
        {
            if (!letters.Any())
            {
                yield break;
            }

            bool lineOk = true;

            foreach (var letter in letters)
            {
                bool endOfLine = letter.makeup.HasFlag(MAKEUP.R_ENDOFLINE);

                // the first long space means that garbage characters will come.
                if (letter.code.Equals(' ') && letter.width > letter.height * 2)
                {
                    lineOk = false;
                }

                if (lineOk)
                {
                    yield return letter;
                }

                // end of line
                if (endOfLine)
                {
                    if (!lineOk)
                    {
                        yield return new LETTER { makeup = MAKEUP.R_ENDOFLINE, code = ' ' };
                    }

                    lineOk = true;
                }
            }
        }
        public static int VerticalDistanceFrom(this LETTER letter1, LETTER letter2)
        {
            return System.Math.Abs(letter2.left - (letter1.left + letter1.width));
        }

        /// <summary>
        /// Serializes the object to an Xml file
        /// </summary>
        /// <param name="obj">the object to be saved</param>
        /// <param name="encoding">the encoding used in the XmlSerialization.</param>
        /// <returns>the serialized object in Xml.</returns>
        public static string ToXml(this object obj, Encoding encoding)
        {
            var serializer = new XmlSerializer(obj.GetType());
            using (var sw = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings { Encoding = encoding, Indent = true };

                using (var writer = XmlWriter.Create(sw, settings))
                {
                    serializer.Serialize(writer, obj);

                    writer.Flush();
                    return sw.ToString();
                }
            }
        }

        /// <summary>
        /// returns the object as serialized Xml
        /// </summary>
        /// <param name="obj">the object to be serialized.</param>
        /// <returns>an Xml serialized object.</returns>
        public static string ToXml(this object obj)
        {
            return ToXml(obj, Encoding.UTF8);
        }

        /// <summary>
        /// Serializes the object to a JSON object.
        /// </summary>
        /// <param name="obj">the instance</param>
        /// <returns>the serialized object.</returns>
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Deserializes a json object.
        /// </summary>
        /// <typeparam name="T">the destination type.</typeparam>
        /// <param name="jsonSerializedString">the JSON object.</param>
        /// <returns>an instance of T deserialzied from the jsonSerializedString</returns>
        public static T FromJson<T>(this string jsonSerializedString)
        {
            return JsonConvert.DeserializeObject<T>(jsonSerializedString);
        }

        /// <summary>
        /// Gets the words from the LETTER colletion.
        /// </summary>        
        /// <param name="zone">the page isntance</param>
        /// <param name="imageIndex">the image index</param>
        /// <returns>an enumeration of words (text, alternatives and word coordinates)</returns>
        public static IEnumerable<Word> GetWords(this OCRZone zone, IMAGEINDEX imageIndex = IMAGEINDEX.II_CURRENT)
        {
            var start = new LETTER();

            var wordList = new List<LETTER>();

            foreach (var letter in zone.Page[imageIndex].GetLetters().Where(letter => letter.zone == zone.Index && letter.code != ' '))
            {
                if (!wordList.Any())
                {
                    start = letter;
                }

                wordList.Add(letter);

                if (!letter.makeup.HasFlag(MAKEUP.R_ENDOFWORD) && !letter.makeup.HasFlag(MAKEUP.R_ENDOFLINE) &&
                    !letter.makeup.HasFlag(MAKEUP.R_ENDOFZONE) && !letter.makeup.HasFlag(MAKEUP.R_ENDOFPARA) &&
                    !letter.makeup.HasFlag(MAKEUP.R_ENDOFCELL)) continue;
                var alternatives = new List<string>();

                var firstLetter = wordList.First();

                if (firstLetter.cntSuggestions > 0)
                {
                    var words = zone.Page.Suggestion.Skip((int)firstLetter.ndxSuggestions + 1);

                    for (var i = 0; i < firstLetter.cntSuggestions; i++)
                    {
                        var word = words.TakeWhile(c => !c.Equals('\0'));

                        alternatives.Add(new string(word.ToArray()));

                        words = words.Skip(2 + word.Count());
                    }
                }

                yield return new Word
                {
                    Text = new string(wordList.Select(c => c.code).ToArray()),
                    Rectangle = new Rectangle(start.left, wordList.Min(x => x.top), letter.left + letter.width - start.left, wordList.Max(x => x.top + x.height) - wordList.Min(x => x.top)),
                    Alternatives = alternatives.ToArray()
                };

                wordList.Clear();
            }
        }
    }
}
