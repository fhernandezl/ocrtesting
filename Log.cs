﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using PD3.PolicyManager.Entities;
using Banco.PD3.Persistence;
using Banco.PD3.Persistence.Entities;
using Banco.PD3.Persistence.Enums;
using System.Data.SqlClient;

namespace Com.BancoAzteca.OCR
{
    class clsLog
    {
        public static SqlDataReader SqlDataReader;
        public static PD3Connection ConexionBaseDatos;
        public static SqlPersister persistence;
        public static int IdParametroEncendidoLogBD = 264;


        public static void ArchiveLog(String Cadena)
        {
            if (OCRProcessor.shouldlog){


                StreamWriter sw;
                try
                {
                    String RutaLogs = @"K:\NET64_BAZ\DIGITALIZACION\Logs\" + Dns.GetHostName() + "_"
                        + "TemplateMatching.log";

                    if (File.Exists(RutaLogs) == false)
                        sw = new StreamWriter(RutaLogs, false);
                    else
                    {
                        FileInfo PesoArchivo = new FileInfo(RutaLogs);

                        if (PesoArchivo.Length > 30000)
                            sw = new StreamWriter(RutaLogs, false);
                        else
                            sw = new StreamWriter(RutaLogs, true);
                    }

                    sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " ----- " + Cadena);
                    sw.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("ArchiveLog --- Exception: " + e.Message);
                }
            }

           
        }

        public static void BDLog(int EstatusLog, String Aplicacion, String Modulo, String DescError)
        {
            if(OCRProcessor.shouldlog)
                RegistrarLogBD(EstatusLog, Aplicacion, Modulo, DescError, EstatusLog);
        }

        public static void BDLog(int EstatusLog, String Aplicacion, String Modulo, String DescError, int OtherId)
        {
            if (OCRProcessor.shouldlog)
                RegistrarLogBD(EstatusLog, Aplicacion, Modulo, DescError, OtherId);
        }

        private static void RegistrarLogBD(int EstatusLog, String Aplicacion, String Modulo, String DescError, int OtherId)
        {
            if (OCRProcessor.shouldlog)
            {

                int ParametroBd = 0;
                try
                {
                    //if (File.Exists(@"K:\NET64_BAZ\Certs\baz-fc08b09b-3801-4bd1-bb74-8571ad2284b4.pd3"))
                    //{

                    //}
                    String path = @"K:\NET64_BAZ\Certs\";
                    String certificado = "baz-fc08b09b-3801-4bd1-bb74-8571ad2284b4.pd3";
                    ConexionBaseDatos = Certificater.GetPD3Connection(path, certificado);
                    persistence = new SqlPersister(ConexionBaseDatos);

                    System.Data.SqlClient.SqlDataReader LogSqlDataReader = persistence.ScriptToDataReader("SELECT fiPrmStatus FROM CATDigParametros WITH(NOLOCK) WHERE fiPrmId = " + IdParametroEncendidoLogBD);
                    if (LogSqlDataReader.HasRows)
                    {
                        LogSqlDataReader.Read();
                        ParametroBd = Convert.ToInt16(LogSqlDataReader["fiPrmStatus"]);
                    }

                    if (ParametroBd > 0)
                    {
                        LogSqlDataReader = persistence.ScriptToDataReader("EXEC PACRGLIDigBitacoraErrores "
                        + "'" + "TemplateMatching" + "','" + OtherId + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + Aplicacion + "',"
                        + "'" + Modulo + "','" + EstatusLog + "','" + DescError.Replace('\'', '"') + "','" + Dns.GetHostName() + " ----- "
                        + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " --- Ver. 1.0.2" + "'");
                    }
                    LogSqlDataReader.Close();
                }
                catch (Exception ex)
                {
                    ArchiveLog("BDLog --- Registro en BD Fallido");
                    ArchiveLog("BDLog --- Mensaje de error: '" + ex.Message + "'");
                }
                finally
                {
                    //if (ParametroBd > 0)
                    //ArchiveLog("BDLog --- Componente: " + Constantes.DatosGenerales.CurrentComponenteBD + " | Clase: " + Aplicacion + " | Método: " + Modulo);
                }
            }

            
        }
    }
}
